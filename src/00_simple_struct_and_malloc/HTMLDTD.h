#ifndef __HTMLDTD_H__
#define __HTMLDTD_H__

#define MAXCHAR 0x100
#define MAXCONT 0x1000
#define MAXCHLD 0x1000

enum HTMLElementType { html, head, body, script, h1, p };

struct ChldStack {
    struct HTMLElement *data[MAXCHLD];
    int summit;
};

struct HTMLElement {
    char id[MAXCHAR];
    enum HTMLElementType type;
    char content[MAXCONT];
    struct HTMLElement *parent;
    struct Head *child_head;			//El hijo head
    struct ChldStack child;
};

struct Head{
	char id[MAXCHAR];
};

#endif
