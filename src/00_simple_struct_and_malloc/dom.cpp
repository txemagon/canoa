/*
 * =====================================================================================
 *
 *       Filename:  dom.cpp
 *
 *    Description:  Creates a simple dom tree
 *
 *        Version:  1.0
 *        Created:  22/06/19 09:55:59
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  txemagon (imasen), txema.gonz@gmail.com
 *   Organization:  Salesianos Domingo Savio
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#include "HTMLDTD.h"

//Funcion para meter datos en la ChldStack
void push (struct ChldStack *stack, struct HTMLElement *nuevo){
	stack->data[stack->summit] = nuevo;
	stack->summit++;
}

//Funcion rellenar valores iniciales
void initial_data (struct HTMLElement *document_root){
    document_root->id[0] = 0;
    document_root->type = html;
    document_root->content[0] = 0;

}

int
main ()
{
    struct HTMLElement *document_root;
    struct Head *document_head;

    // document_root = ... creo con un malloc la estructura.
    document_root = (HTMLElement *) malloc(sizeof(document_root));
    // relleno con los valores iniciales los campos de document_root (si es posible con una función).
    initial_data(document_root);

    // Creo con un malloc el hijo head para document_root
    document_head = (Head *) malloc(sizeof(document_head));
    // Relleno los campos iniciales
    // Me aseguro que el campo parent apunta a document_root
    // Me aseguro que document_root.child.data[0] apunta al malloc recién creado.
    document_root->child.data[0];

    // Libero todos los hijos child de document_root
    free (document_root);
    free (document_head);
    
    return EXIT_SUCCESS;
}


