#include <stdio.h>
#include <curl/curl.h>

#define ARCHIVO "pagina.txt"
#define CAR 200

int main()
{
  CURL *curl;
  CURLcode res;
  curl = curl_easy_init();
  FILE *fp =  fopen(ARCHIVO, "wb");
  char a[CAR];
  printf("Indica la URL de la pagina:\n"); //Example https://www.youtube.com
  scanf("%s", a);

  if(curl) {
   curl_easy_setopt(curl, CURLOPT_URL, a);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);

    res = curl_easy_perform(curl);
    /* Comprueba errores */
    if(res != CURLE_OK)
      fprintf(stderr, "curl_easy_perform() failed: %s\n",
              curl_easy_strerror(res));
    curl_easy_cleanup(curl);
  }
  return 0;
}


// pagina info https://curl.haxx.se/libcurl/c/curl_easy_setopt.html
// Instalar libreria antes, porque si no da fallo --> sudo apt-get install libcurl4-openssl-dev
// Compilacion --> g++ -o example example.cpp -lcurl
