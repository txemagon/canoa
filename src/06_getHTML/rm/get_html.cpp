#include <stdio.h>
#include <curl/curl.h>

#define ARCHIVO "pagina.txt"
#define MAX 200

void get_html(){
    CURL *curl;
    CURLcode res;
    curl = curl_easy_init();
    FILE *fp =  fopen(ARCHIVO, "wb");
    char url[MAX];

    if(curl) {
        curl_easy_setopt(curl, CURLOPT_URL, url);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);

        res = curl_easy_perform(curl);

        check_errors_get_html(res);

        curl_easy_cleanup(curl);
    }

}

void check_errors_get_html(CURLcode res){
    if(res != CURLE_OK)
        fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
}
