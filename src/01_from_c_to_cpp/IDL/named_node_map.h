#ifndef __NAMED_NODE_MAP_H__
#define __NAMED_NODE_MAP_H__

#include "classes.h"


class NamedNodeMap
{
  Node               getNamedItem(DOMString name);
  Node               setNamedItem(Node arg);               //raises(DOMException);
  Node               removeNamedItem(DOMString name);      //raises(DOMException);
  Node               item(unsigned long index);

  unsigned long      length;
  // Introduced in DOM Level 2:
  Node               getNamedItemNS(DOMString namespaceURI, DOMString localName);
  // Introduced in DOM Level 2:
  Node               setNamedItemNS(Node arg);                //raises(DOMException);
  // Introduced in DOM Level 2:
  Node               removeNamedItemNS(DOMString namespaceURI, DOMString localName);       //raises(DOMException);
};

#endif
