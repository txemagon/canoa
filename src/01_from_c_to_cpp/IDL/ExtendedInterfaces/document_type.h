#ifndef __DOCUMENT_TYPE_H__
#define __DOCUMENT_TYPE_H__

class DocumentType : Node {
	
	DOMString		*name;
	NamedNodeMap	*entities;
	NamedNodeMap	*notations;
	// Introduced in DOM Level 2:
	DOMString       *publicId;
	// Introduced in DOM Level 2:
	DOMString        *systemId;
	// Introduced in DOM Level 2:
	DOMString        *internalSubset;
	
};




#endif
