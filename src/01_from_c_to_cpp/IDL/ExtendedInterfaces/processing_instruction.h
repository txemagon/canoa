#ifndef __PROCESSING_INSTRUCTION_H__
#define __PROCESSING_INSTRUCTION_H__

#include "../node.h"

class ProcessingInstruction : public Node {
	DOMString		target;
	DOMString		data;			// raises(DOMException) on setting
};


#endif
