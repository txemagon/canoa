#include "node.h"

Node::Node() :
  namespaceURI ("Hay que poner un valor porque es const"),
  prefix ("Hay que poner un valor porque es const"),
  localName ("Hay que poner un valor porque es const")
{

}

Node Node::insertBefore (Node newChild, Node refChild)	// raises(DOMException)
{
  Node n;
  	//Si refChild es NULL, se inserta al final de la lista de hijos
	//ELSE
	//Inserta el newChild antes del refChild
	//Si ya existe newChild se elimina el primero
	//Retorna el Nodo que va a ser insertado
  return n;
}

Node Node::replaceChild (Node newChild, Node oldChild)	// raises(DOMException)
{
  Node n;
  //Buscar nodo anterior
  if (childNodes.node = oldChild)
	  newChild.childNodes = oldChild.childNodes;
  else
	  childNodes.node = childNodes.next;


  return n;
}

Node Node::removeChild (Node oldChild)	// raises(DOMException)
{
  Node n;
  	//Elimina el nodo indicado(oldChild) de la lista de hijos
	//Se retorna el nodo que va a ser eliminado
  return n;
}

Node
Node::appendChild (Node newChild)	// raises(DOMException)
{
  Node n;
  	//Si newChild está en la lista de hijos se elimina
	//ELSE
	//Inserta newChild al final de la lista de hijos de este nodo
	//Se retorna el nodo añadido
  return n;
}

bool
Node::hasChildNodes ()
{
	if (childNodes.next == NULL)
		return true;
  return false;
}

Node
Node::cloneNode (bool deep)
{
  Node n;
  	//Nodo duplicado no tiene padre
	//Copia todos los atributos y valores
	//deep true si clona recursivamente el subarbol, false si solo clona el nodo
	//retorna el nodo duplicado
  return n;
}


void
Node::normalize ()		// Modified in DOM Level 2:
{
}

bool
Node::hasAttributes ()
{
  return true;
}
