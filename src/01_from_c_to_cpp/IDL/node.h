#ifndef __NODE_H__
#define __NODE_H__

#include "classes.h"

#include "dom_string.h"
#include "node_list.h"
#include "named_node_map.h"


class Node {

  // NodeType
  static const unsigned short      ELEMENT_NODE                   = 1;
  static const unsigned short      ATTRIBUTE_NODE                 = 2;
  static const unsigned short      TEXT_NODE                      = 3;
  static const unsigned short      CDATA_SECTION_NODE             = 4;
  static const unsigned short      ENTITY_REFERENCE_NODE          = 5;
  static const unsigned short      ENTITY_NODE                    = 6;
  static const unsigned short      PROCESSING_INSTRUCTION_NODE    = 7;
  static const unsigned short      COMMENT_NODE                   = 8;
  static const unsigned short      DOCUMENT_NODE                  = 9;
  static const unsigned short      DOCUMENT_TYPE_NODE             = 10;
  static const unsigned short      DOCUMENT_FRAGMENT_NODE         = 11;
  static const unsigned short      NOTATION_NODE                  = 12;

protected:
  DOMString        nodeName;
  DOMString        nodeValue;
                                        // raises(DOMException) on setting
                                        // raises(DOMException) on retrievala
  unsigned short   nodeType;
  Node             const *parentNode;
  NodeList         childNodes;
  Node             const *firstChild;
  Node             const *lastChild;
  Node             const *previousSibling;
  Node             const *nextSibling;
  NamedNodeMap     attributes;
  // Modified in DOM Level 2:
  //Document         ownerDocument;
  Node               insertBefore(Node newChild,
                                  Node refChild); // raises(DOMException)
  Node               replaceChild(Node newChild,
                                  Node oldChild); // raises(DOMException)
  Node               removeChild(Node oldChild);  // raises(DOMException)
  Node               appendChild(Node newChild);  // raises(DOMException);
  bool               hasChildNodes();
  Node               cloneNode(bool deep);
  // Modified in DOM Level 2:
  void               normalize();
  // Introduced in DOM Level 2:
  bool               isSupported(DOMString feature,
                                 DOMString version);
  // Introduced in DOM Level 2:
  DOMString         const namespaceURI;
  // Introduced in DOM Level 2:
  DOMString         const prefix;
                                        // raises(DOMException) on setting

  // Introduced in DOM Level 2:
  DOMString          const localName;
  // Introduced in DOM Level 2:
  bool            hasAttributes();

public:
  Node();
};


#endif
