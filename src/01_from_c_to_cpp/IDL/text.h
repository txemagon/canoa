#ifndef __TEXT_H__
#define __TEXT_H__

#include "character_data.h"

class Text : public CharacterData {
	Text	splitText(unsigned long offset);		// raises(DOMException)
};

#endif
