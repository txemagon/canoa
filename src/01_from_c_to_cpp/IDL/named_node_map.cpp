#include "named_node_map.h"
#include "node.h"

Node
NamedNodeMap::getNamedItem(DOMString name)
{
    Node n;

    return n;

}

Node
NamedNodeMap::setNamedItem(Node arg)               //raises(DOMException);
{
	Node n;

	return n;
}

Node
NamedNodeMap::removeNamedItem(DOMString name)      //raises(DOMException);
{

    Node n;

    return n;
}

Node
NamedNodeMap::item(unsigned long index)
{

    Node n;

    return n;
}


  // Introduced in DOM Level 2:
Node
NamedNodeMap::getNamedItemNS(DOMString namespaceURI, DOMString localName)
{

    Node n;

    return n;
}

// Introduced in DOM Level 2:
Node
NamedNodeMap::setNamedItemNS(Node arg)                //raises(DOMException);
{

    Node n;

    return n;
}

// Introduced in DOM Level 2:
Node
NamedNodeMap::removeNamedItemNS(DOMString namespaceURI, DOMString localName)
{

    Node n;

    return n;
}