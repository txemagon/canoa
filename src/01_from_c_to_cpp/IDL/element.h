#ifndef __ELEMENT_H__
#define __ELEMENT_H__

#include "attr.h"

class Element : Node {
	
	DOMString 		tagName;
	DOMString		getAttribute(DOMString name);
	void 			setAttribute(DOMString name, DOMString value);		//raises DOMException
	
	Attr			getAttributeNode(DOMString name);
	Attr			setAttributeNode(Attr newAttr);				//raises DOMException
	Attr			removeAttributeNode(Attr oldAttr);			//raises DOMException
	
	NodeList		getElementByTagName(DOMString name);
	// Introduced in DOM Level 2:
	DOMString		getAttributeNS(DOMString namespaceURI, DOMString localName);

	// Introduced in DOM Level 2:
	void 			setAttributeNS(DOMString namespaceURI, DOMString qualifiedName, DOMString value);	//raises DOMException
 
	// Introduced in DOM Level 2:
	void 			removeAttributeNS(DOMString namespaceURI, DOMString localName);			//raises DOMExcepcion
	 
	// Introduced in DOM Level 2:
	Attr               getAttributeNodeNS(DOMString namespaceURI, DOMString localName);
	// Introduced in DOM Level 2:
	Attr               setAttributeNodeNS(Attr newAttr);		//raises(DOMException);
	// Introduced in DOM Level 2:
	NodeList           getElementsByTagNameNS(DOMString namespaceURI, DOMString localName);
	// Introduced in DOM Level 2:
	bool	           hasAttribute(DOMString name);
	// Introduced in DOM Level 2:
	bool	            hasAttributeNS(DOMString namespaceURI, DOMString localName);
		
	
};


#endif


