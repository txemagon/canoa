#ifndef __CHARACTER_DATA_H__
#define __CHARACTER_DATA_H__

#include "node.h"

class CharacterData : Node {
	DOMString	data;
			// raises(DOMException) on setting
            // raises(DOMException) on retrieval
    unsigned long 	lenght;
    DOMString		substringData(unsigned long offset, unsigned long count);		//raises DOMException
    
    void 			appendData(DOMString arg);													//raises DOMException
	void 			insertData(unsigned long offset, DOMString arg);							//raises DOMException
	void 			deleteData(unsigned long offset, unsigned long count);						//raises DOMException
	void 			remplaceData(unsigned long offset, unsigned long count, DOMString arg);		//raises DOMException
		
            
};

#endif
