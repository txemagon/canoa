#ifndef __DOM_STRING_H__
#define __DOM_STRING_H__

#include <string>

using namespace std;

class DOMString
{
    string text;
    public:
        DOMString(const char *initial_text = "");
        DOMString(string initial_text);

};

#endif
