#ifndef __DOCUMENT_H__
#define __DOCUMENT_H__
#include "ExtendedInterfaces/document_type.h"
#include "dom_implementation.h"
#include "element.h"
#include "document_fragment.h"
#include "text.h"
#include "comment.h"
#include "attr.h"

//#include "ExtendedInterfaces/cdata_section.h"

#include "ExtendedInterfaces/processing_instruction.h"
#include "ExtendedInterfaces/cdata_section.h"
#include "ExtendedInterfaces/document_type.h"
#include "ExtendedInterfaces/entity_reference.h"


class Document : Node{

	protected:
	DocumentType		*doctype;
	DOMImplementation		*implementation;
	Element		*documentElement;
	Element		createElement(DOMString tagName);		//raises(DOMException)

	DocumentFragment		createDocumentFragment();
	Text		createTextNode(DOMString data);
	Comment		createComment(DOMString data);
	CDATASection		createCDTASection(DOMString data); 	//raises(DOMException)

	ProcessingInstruction		createProccessingInstruction(DOMString tarjet, DOMString data);		//raises(DOMException)

	Attr					createAtribute(DOMString name);			//raises(DOMException)

	//EntityReference			createEntityReference(DOMString name);	//raises(DOMException)

	NodeList				getElementByTagName(DOMString tagname);
	// Introduced in DOM Level 2:
	Node			importNode(Node importedNode, bool deep);		//raises(DOMException)
	// Introduced in DOM Level 2:
	Element			createElementNS(DOMString namespaceURI, DOMString qualifiedName);		//raises(DOMException)
	// Introduced in DOM Level 2:
	Attr			createAttributeNS(DOMString namespaceURI, DOMString qualifiedName);
	// Introduced in DOM Level 2:
	NodeList		getElementsByTagNameNS(DOMString namespaceURI, DOMString localName);
	// Introduced in DOM Level 2:
	Element			getElementById(DOMString elementId);


};


#endif
