#ifndef __NODE_LIST_H__
#define __NODE_LIST_H__

class NodeList
{
public:
	int id;
	Node* next;
        Node* parent;
};

bool hasNext();

#endif
