#!/bin/bash
sudo apt install -y libncurses5-dev libncursesw5-dev
sudo apt install -y libcurl4-openssl-dev
sudo apt install -y make
sudo apt install -y g++
git clone https://gitlab.com/txemagon/canoa/
cd canoa/src/99_Implement
rm -rf ej_canoa
make
mv ej_canoa ../../../
