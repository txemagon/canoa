#include <stdio.h>
#include <stdlib.h>

#include "definitions.h"

using namespace std;

int
main()   {
    List *nodes_list = new List();
    List *child_list = new List();
    List *child_list2 = new List();

    Node *node1 = new Node();

    node1->id = 1;
    node1->childList = child_list;
    Node *child = new Node();
    child->id = 11;
    node1->childList->insertNodeAtEnd(child);
    Node *node2 = new Node();
    node2->id = 2;
    Node *node3 = new Node();
    node3->id = 200;
    child_list2->insertNodeAtEnd(node3);
    child->childList = child_list2;


    stack *pila = new stack();

    nodes_list->insertNodeAtEnd(node1);
    nodes_list->insertNodeAtEnd(node2);

    nodes_list->printList();

    recorre_list(nodes_list, pila);			//Metodo que hace push de los datos de la lista

    return EXIT_SUCCESS;
}
