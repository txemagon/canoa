#include <stdio.h>
#include <stdlib.h>
#include "node.h"
#include "list.h"
#include "dom_string.h"
#define MAX_HIJOS 0x100

struct DomString;

Node::Node() {
    id = 0;
    sgte = NULL;
    prev = NULL;
    childList = NULL;
}
Node::Node(char *nombreNodo) {
    id = 0;
    sgte = NULL;
    prev = NULL;
    childList = NULL;
    nodeName = nombreNodo;
}


Node Node::cloneNode(bool deep) {
    Node *temp = new Node();
    if(deep){
        //Clona recursivamente el nodo
    }else{
        //Tan solo se clona todo lo del nodo en sí
    }
    return *temp;
}


/*CHILD FUNCTIONS*/
Node
Node::insertBefore(Node *newChild, Node *refChild){
    if (childList != NULL) {
        Node *temp = new Node();
        temp = childList->ult_nodo;
        if (childList->searchNode(refChild)) {  //Si refChild está en la lista
            newChild->prev = refChild->prev;		//Previo del refChild es ahora el del newChild
            refChild->prev = newChild;				//Previo del refChild es la direccion del newChild
            newChild->sgte = refChild;				//Siguiente del newChild es la direccion del refChild
            childList->n_nodos++;
        }
        else {
            //Insertamos el nodo al final de la lista hijos
            childList->insertNodeAtEnd(newChild);
        }
    }
    return *newChild;
}

Node
Node::remplaceChild(Node *newNode, Node *oldNode) {
    Node *current = new Node();
    current = this->childList->ult_nodo;	//nodo auxiliar es el ultimo nodo de la lista hijos
    if (this->childList->searchNode(oldNode)) {
        while (current != NULL) {			//Recorremos la lista actualizando el nodo auxiliar(current)
            if (current == oldNode) {		//Lo encontramos y lo remplazamos por el nuevo
                if (oldNode->prev != NULL) {			//Si el nodo tiene previo entra
                    Node *temp = new Node();
                    temp = oldNode->prev;
                    newNode->prev = oldNode->prev;
                    temp->sgte = newNode;
                }

                if (oldNode->sgte != NULL) {			//Si el nodo tiene siguiente entra
                    Node *temp = new Node();
                    temp = oldNode->sgte;
                    newNode->sgte = oldNode->sgte;
                    temp->prev = newNode;
                }
                if (oldNode == this->childList->ult_nodo) {		//En caso de ser el ultimo nodo debemos actualizar el puntero de la lista tambien
                    this->childList->ult_nodo = newNode;
                }
            }
            current = current->prev;
        }

    }
    return *newNode;
}

bool
Node::hasChildNodes() {
    if(childList != NULL)
        if(childList->n_nodos > 0)
           return true;
        else
            return false;
    else
        return false;
}


Node
Node::removeChild(Node *oldChild)	// raises(DOMException)		//FALTA TESTEAR
{
	Node *current = new Node();
	current = this->childList->ult_nodo;
	if (this->hasChildNodes()) {
		if (this->childList->searchNode(oldChild)) {
			while (current != NULL) {
				if (current == oldChild) {
					Node *previo = new Node();
					Node *siguiente = new Node();
					previo = current->prev;
					siguiente = current->sgte;
					if (previo != NULL)
						oldChild->prev = previo;

					if (siguiente != NULL)
						oldChild->sgte = siguiente;
					childList->n_nodos--;
				}
			}
		}
	}
	return *oldChild;
}


bool 
Node::isSupported(DomString *feature, DomString *version)
{
	return true;
}

void
Node::normalize()		// Modified in DOM Level 2:
{
}


bool
Node::hasAttributes()
{
	return true;
}
