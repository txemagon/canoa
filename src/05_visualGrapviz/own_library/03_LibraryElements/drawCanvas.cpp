#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <fstream>
#include "stack.h"
#include "drawCanvas.h"
#include <unistd.h>

#define NOMBRE "frame.dot"

using namespace std;

string unionNodes(string node1, string node2) {
	string arrow = "->";
	string result = node1 + arrow + node2 + "\n";
	return result;
}

void generateGraph(stack *p) {

	ofstream file;
	file.open(NOMBRE);

	file << "digraph{\n";

	for (int cnt = 0; cnt<p->cima; cnt++)
		file << p->dato[cnt];

	file << "}";

	file.close();
	system("./script.sh");
        sleep(2);
}
