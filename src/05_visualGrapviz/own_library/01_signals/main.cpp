/*COMPILAR
 * 1º make hijo.cpp
 * 2º make main.cpp*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define NUM_SIGNALS 3
#define NOMBRE "frame.dot"

const char *program = "./hijo";

int spawn () {
    pid_t child_pid;
    child_pid = fork ();  /*Creamos el fork para dividir el programa en 2 procesos
                           iguales con distinto valor de retorno*/
    if(child_pid !=0)
        return child_pid;
    //ELSE
     execvp (program, NULL);  /*Arrancamos el programa hijo del otro archivo "hijo.cpp"*/
     fprintf (stderr, "Un error ocurrio en execvp\n");
     abort ();

}

void makeGraph(char* body){
    FILE *fichero;

    if(!(fichero = fopen(NOMBRE,"w")))
        fprintf(stderr,"Fail to open file");

    char* header = "digraph{";
    char* end = "}";


    fichero = fopen(NOMBRE, "w");

    fprintf(fichero,header);
    fprintf(fichero,body);
    fprintf(fichero,end);

    fclose(fichero);
}

int main ()
{
    pid_t child_pid;

    int child_status;

    child_pid = spawn ();   /*Guardamos el identificador del hijo
                            que retorna spawn y a la vez llamamos a la función*/
    sleep(1);
    makeGraph("a->c");
    kill(child_pid, SIGUSR1);
    printf("Señales Enviadas\n");

    sleep(1);
    makeGraph("b->c");
    kill(child_pid, SIGUSR1);
    printf("Señales Enviadas\n");

    sleep(1);
    makeGraph("c->d");
    kill(child_pid, SIGUSR1);
    printf("Señales Enviadas\n");



    wait(&child_status);    /*Esperamos a que el hijo acabe para posteriormente
                            limpiarlo y evitar que se quede Zombie*/

    if(!WIFEXITED (child_status))
        printf ("El proceso hijo no ha salido bien\n");

    return EXIT_SUCCESS;
}

