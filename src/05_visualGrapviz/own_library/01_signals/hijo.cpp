#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>

sig_atomic_t sigusr1_count = 0;

void handler (int signal_number){
    ++sigusr1_count;
    printf("Señar recibida");
    system("./script.sh");
}

int main(){
     struct sigaction sa;

     memset(&sa, 0, sizeof(sa));
     sa.sa_handler = &handler;
     sigaction (SIGUSR1, &sa, NULL);  /*Manejador de señales encargado de recibir las señales que le
                                       mandamos desde el padre*/

     while(sigusr1_count < 3){         /*Constantemente comprueba si le hemos enviado ya las 3 señales*/
         sleep(1);
     }

     return EXIT_SUCCESS;
}
