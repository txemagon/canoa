#ifndef __INTERFACE_H__
#define __INTERFACE_H__

#include <ncurses.h>
#include <stdlib.h>


WINDOW *create_newwin(int height, int width, int start_y, int start_x);
void check_errors_get_html(CURLcode res);
void Carga();
void url_window();
void maximum_screen();
void menu_window();

#endif
