#include "definitions.h"

/*Variables globales*/
int altura = 3, ancho = 100, y = 1, x = 18;
int height = 0, width = 0, start_y = 0, start_x = 0;
#define A 100
#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0])) /* variables para el menu*/
#define CTRLD   4
#define ARCHIVO "pagina.txt" /*Archivo donde pone el HTML que hemos solicitado*/


/*crear un switch con char * */
constexpr unsigned int str2int(const char* str, int h = 0)
{
    return !str[h] ? 5381 : (str2int(str, h+1) * 33) ^ str[h];
}

/*En esta parte indicamos las opciones que estaran en el recuadro de la funcion menu*/
const char *opciones[] = {
    "-ver web",
    "-ver estructura dom",
    "-ver etiquetas totales",
    "-ver posibles errores del html",
    "-Salir con F2 con F2",
    (char *)NULL,
};


/*>>>FUNCIONES<<<<*/

/*funcion que crear una caja segun las medidas que le indiques*/
WINDOW *create_newwin(int height, int width, int start_y, int start_x){
    WINDOW *win;

    win = newwin(height, width, start_y, start_x);
    box(win, 0, 0);

    wrefresh(win);

    return win;
}

/*Comprueba si la petición si la petición a la pagina solicitada a devuelto el html o no*/
void check_errors_get_html(CURLcode res){
    WINDOW *my_win;
    char msg_error[A] = "Error no se ha podido enviar la petición a la pagina solicitada";
    char msg_V[A] = "Cargando pagina solicitada...";

    if(res != CURLE_OK){
/*    fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));*/
    my_win = create_newwin(3, 100, 20, 40);
    mvwprintw(my_win, 1,20, msg_error);
    }
    else{
        my_win = create_newwin(3,100,20,40);
        mvwprintw(my_win, 1,20, msg_V);
    }
    wrefresh(my_win);

}



/*
 * Barra de carga
 */
void barra_de_carga()
{
    printf("\n");
    sleep(3);
    /*printf(ANSI_COLOR_YELLOW "\nObteniendo HTML de la pagina solicitada\n" ANSI_COLOR_RESET);*/
    for (int vez=0; vez<80; vez++){
        printf("\r");
        for (int i=0; i<vez; i++)
           // mvprintw(40,40," %s","▓");
            printf("▓");
        fflush(stdout);
        usleep(5000);
    }
}


/*Función que genera una especie de cuadrado por los maximo de la terminal*/
void maximum_screen(){
    int height=0, width=0, start_y= 0, start_x = 0;
    WINDOW *my_win;
    int row, col;

    my_win = create_newwin(height, width, start_y, start_x);

    wrefresh(my_win);

}


/*En esta funcion tenemos el mensaje de ingreso de URL y la ventana
 * donde se indica la url por parte del usuario, donde a su vez
 * tambien se encuentra la función que comprueba si se ha encontrado
 * un error a la hora de hacer la petición a la URL indicada por el usuario
 * dependiendo de esa petición muestra una ventana con error o
 * un mensaje de que todo a salido correctamente
 */
void url_window(){
    WINDOW *win2;

    char msg[] = "Ingrese una URL: ";
    char URL[A];
    CURL *curl;
    CURLcode res;
    FILE *fp = fopen("ARCHIVO", "wb");
    int ch;
    keypad(stdscr, TRUE);

    mvprintw(2,2,"%s", msg); /*Se imprime el mensaje, en la posicion indicada*/

    win2 = create_newwin(altura, ancho, y, x); /*Se crea otra caja donde el usuario ingresa la URL*/
    //scroll(win2);
    curl = curl_easy_init();

    mvwprintw(win2, 1, 19, URL);

    /*ch = getch();
    if (ch == KEY_F(1)) // Pruebas para abrir el menu con F1
        menu_window();
    //refresh();
    */
    getstr(URL); /* con getstr obtenemos la cadena que el usuario ingrese en este caso la URL*/

    if (curl) {
        curl_easy_setopt(curl, CURLOPT_URL, URL);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);

        res = curl_easy_perform(curl);

        check_errors_get_html(res);

        curl_easy_cleanup(curl);

    }
}

/*Esta función imprime un cuadro con diferentes opciónes
 *que el usuario podra ver a partir del html creado una
 *vez enviada la petición a la pagina
 */
void menu_window(){
    /*variables menu*/
    ITEM **my_items;
    int c;
    MENU *my_menu;
    ITEM *cur_item;
    WINDOW *new_win;
    int n_choices, i;


    initscr();
    cbreak();
    noecho(); //Para que no permita escribir
    keypad(stdscr, TRUE);


    n_choices = ARRAY_SIZE(opciones);
    my_items = (ITEM **)calloc(n_choices, sizeof(ITEM *));
    for(i = 0; i < n_choices; ++i){
        my_items[i] = new_item(opciones[i],""); /*Se indica a vacio, porque si no crearia 2 veces las opciones.*/
    }

    my_menu = new_menu((ITEM **)my_items);
//    mvprintw(40, 0, "F1 para salir");

    new_win = newwin(13,40,15,77); // cuadrado creado para el menu
    keypad(new_win, TRUE);
    //box(new_win, 0,0);

    set_menu_win(my_menu, new_win);
    set_menu_sub(my_menu, derwin(new_win, 6,38,3,1)); // asignamos el menu al cuadrado indicandole con valores

    set_menu_format(my_menu,5,1);
    set_menu_mark(my_menu, "*");

    box(new_win, 0,0);
    menu_opts_off(my_menu, O_SHOWDESC);
    refresh();

    post_menu(my_menu);
    wrefresh(new_win);

    /*Se ejecuta mienntras usamos las flechas hacia arriba o hacia abajo y dependiendo
     * de la opción que se elija el switch hace una u otra cosa de momento le ponemos
     * esas opciones pero en cada opción tendra asignada la tarea indicada por la misma
     * opción
     */
    while((c = getch()) != KEY_F(2))
    {

        switch(c)
        {
            case KEY_DOWN:
                menu_driver(my_menu, REQ_DOWN_ITEM);
                break;
            case KEY_UP:
                menu_driver(my_menu, REQ_UP_ITEM);
                break;
            case 10:
                switch(str2int(item_name(current_item(my_menu))))
                {
                    case str2int("-ver web"):
                        clear();
                        mvprintw(20,0,"Pagina HTML");
                        break;
                    case str2int("-ver estructura dom"):
                        clear();
                        mvprintw(20,0,"Estructura DOM de la pagina HTML solicitada en la URL");
                        break;
                    case str2int("-ver etiquetas totales"):
                        clear();
                       mvprintw(20,0,"Demostracion de las etiquetas de la pagina html");
                       break;
                    case str2int("-ver posibles errores del html"):
                        clear();
                        mvprintw(20,0,"Aqui se mostraran acontinuacion los errores generados por el html");
                        break;
                    case str2int("-Salir con F2"):
                        exit(1);
                        break;
                        wrefresh(new_win);
                }
                break;
                wrefresh(new_win);
            default:
                break;
         }
                wrefresh(new_win);
    }
    refresh();
    wrefresh(new_win);

    unpost_menu(my_menu);
    free_menu(my_menu);
    for(i=0; i<n_choices; i++)
        free_item(my_items[i]);

    endwin();

}



