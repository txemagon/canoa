#include <ncurses.h>

int main() {

    initscr(); /* Inicia en la terminal el modo ncurses */
    cbreak();
    printw("Hello world"); /*Imprime por pantalla*/
    refresh(); /*para mostrar por pantalla necesitamos llamar a refresh*/
    getch();/*Espera la entrada del usuario*/
    endwin(); /*Cerramos el modo ncurses*/
    /*halfdelay: se utiliza para habilitar el modo de medio retraso en que puedes escribir: por ejm se puede usar para cuando se desea pedir informacion a un usuario sobre algo y si no repsonde en cierto
     * tiempo podemos hacer una cosa u otra, por ejemplo pedir una contraseña*/

}
