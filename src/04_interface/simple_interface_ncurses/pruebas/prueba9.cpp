#include <ncurses.h>

int main()
{

    WINDOW *p;
    int x,c;

    initscr();

    p = newpad(50, 100);
    if (p == NULL)
    {
        addstr("unable to create new pad");
        refresh();
        endwin();
        return(1);
    }

    addstr("New pad created");
    refresh();

    endwin();
    return 0;
}
