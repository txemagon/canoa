#include <ncurses.h>

int main()
{
    int maxy, maxx, y;


    initscr();
    getmaxyx(stdscr, maxy, maxx);
    scrollok(stdscr, TRUE);

    for(y=0; y<maxy; y++){
        mvprintw(y, 0, "El texto se repiten tantas veces como lineas %d\n", y);
    }

    refresh();
    getch();

    scroll(stdscr);
    refresh();

    scroll(stdscr);
    refresh();

    scroll(stdscr);
    refresh();

    scroll(stdscr);
    refresh();
    getch();

    endwin();
    return 0;
}
