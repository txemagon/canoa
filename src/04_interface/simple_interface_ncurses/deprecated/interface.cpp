#include "definitions.h"


#define A 100
/*funcion que crear una caja segun las medidas que le indiques*/
WINDOW *create_newwin(int height, int width, int start_y, int start_x){
    WINDOW *win;

    win = newwin(height, width, start_y, start_x);
    box(win, 0, 0);

    wrefresh(win);

    return win;
}

/*Comprueba si la petición a la pagina a sido devuelta*/
void check_errors_get_html(CURLcode res){
    WINDOW *my_win;
    char msg_error[A] = "Error no se ha podido enviar la petición a la pagina solicitada";
    char msg_V[A] = "Cargando pagina solicitada...";

    if(res != CURLE_OK){
/*    fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));*/
    my_win = create_newwin(3, 100, 20, 40);
    mvwprintw(my_win, 1,20, msg_error);
    }
    else{
        my_win = create_newwin(3,100,20,40);
        mvwprintw(my_win, 1,20, msg_V);
    }
    wrefresh(my_win);

}



/*
 * Barra de carga
 */
void Carga()
{
    printf("\n");
    sleep(3);
    /*printf(ANSI_COLOR_YELLOW "\nObteniendo HTML de la pagina solicitada\n" ANSI_COLOR_RESET);*/
    for (int vez=0; vez<80; vez++){
        printf("\r");
        for (int i=0; i<vez; i++)
           // mvprintw(40,40," %s","▓");
            printf("▓");
        fflush(stdout);
        usleep(5000);
    }
}

/*Menu de opciones*/
/*
void menu(WINDOW *menu, int highlight){
    int x, y = 2;
    box(menu, 0, 0);
    for (int i=0; i<n_opciones; ++i)
    {
        if(highlight == i + 1)
        {
            wattron(menu, A_REVERSE);
            mvwprintw(menu, y, x, "%s", opciones[i]);
            wattroff(menu, A_REVERSE);
        }
        else
            mvwprintw(menu, y, x, "%s", opciones[i]);
        ++y;
    }
    wrefresh(menu);
}
*/

