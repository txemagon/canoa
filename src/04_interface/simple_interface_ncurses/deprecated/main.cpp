#include "definitions.h" /*se incluye el archivo donde apunta a las funciones*/

#define ARCHIVO "pagina.txt" /*Archivo donde pone el HTML que hemos solicitado*/
#define X 200 /*Maximo de caracteres que se puede poner en la URL */

/*En esta parte indicamos las opciones que estaran en el recuadro de la funcion menu*/
char *opciones[] = {
    "-ver web",
    "-ver estructura dom",
    "-ver etiquetas totales",
    "-ver posibles errores del html",
    "Salir",
};
#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))
#define CTRLD   4

constexpr unsigned int str2int(const char* str, int h = 0)
{
    return !str[h] ? 5381 : (str2int(str, h+1) * 33) ^ str[h];
}


int main() {

    char msg[] = "Ingrese una URL: ";
    char URL[X];
    CURL *curl;
    CURLcode res;
    curl = curl_easy_init();
    FILE *fp = fopen("ARCHIVO", "wb");
    int height = 0, width= 0, start_y = 0, start_x= 0;
    int altura = 3, ancho= 100, y = 1, x = 18;
    /*variables menu*/
    ITEM **my_items;
    int c;
    MENU *my_menu;
    int n_choices, i;
    ITEM *cur_item;
    /*variables de ventana*/
    WINDOW *my_win;
    WINDOW *win2;
    int row, col;

    initscr(); /*iniciar modo ncurses*/
    raw(); /*Permite escribir*/
    refresh();
    getmaxyx(stdscr,row,col);
 //   mvprintw(row-2,0,"La pantalla tiene %d filas y columnas %d\n", row, col);
    my_win = create_newwin(height, width, start_y, start_x); /*Se crea un cuadrado alrededor de la terminal*/
    scrollok(my_win, FALSE);
    mvprintw(2,2,"%s", msg); /*Se imprime el mensaje, en la posicion indicada*/


    win2 = create_newwin(altura, ancho, y, x); /*Se crea otra caja donde el usuario ingresa la URL*/
    //scroll(win2);

    mvwprintw(win2, 1, 19, URL);
    getstr(URL); /* con getstr obtenemos la cadena que el usuario ingrese en este caso la URL*/
    if (curl) {
        curl_easy_setopt(curl, CURLOPT_URL, URL);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);

        res = curl_easy_perform(curl);

        check_errors_get_html(res);

        curl_easy_cleanup(curl);

    }

    //Carga(); /*Hace una barra de carga*/
    sleep(5);
    clear();
    wrefresh(my_win);
    cbreak();
    keypad(stdscr, TRUE);

    /*Menu de opciones opcional por si queremos ver la estructura del html mas afondo*/
    n_choices = ARRAY_SIZE(opciones);
    my_items = (ITEM **)calloc(n_choices + 1, sizeof(ITEM *));

    for(i = 0; i < n_choices; ++i)
        my_items[i] = new_item(opciones[i],opciones[i]);
        my_items[n_choices] = (ITEM *)NULL;

        my_menu = new_menu((ITEM **)my_items);
        mvprintw(40, 0, "F1 para salir");
        post_menu(my_menu);
        refresh();

        while((c = getch()) != KEY_F(1))
        {   switch(c)
            {
                case KEY_DOWN:
                    menu_driver(my_menu, REQ_DOWN_ITEM);
                        break;
                case KEY_UP:
                     menu_driver(my_menu, REQ_UP_ITEM);
                        break;
                case 10:
                        switch(str2int(item_name(current_item(my_menu))))
                        {
                            case str2int("-ver web"):
                                clear();
                                mvprintw(20,0,"Pagina HTML");
                                break;
                            case str2int("-ver estructura dom"):
                                clear();
                                //mvprintw(20,0,"Estructura DOM de la pagina HTML solicitada en la URL");
                                printf("xd");
                                printf("xdd");
                                break;
                            case str2int("-ver etiquetas totales"):
                                clear();
                                mvprintw(20,0,"Demostracion de las etiquetas de la pagina html");
                                break;
                            case str2int("-ver posibles errores del html"):
                                clear();
                                mvprintw(20,0,"Aqui se mostraran acontinuacion los errores generados por el html");
                                break;
                            case str2int("Salir"):
                                break;
                        }


             }
        }

        free_item(my_items[0]);
        free_item(my_items[1]);
        free_menu(my_menu);

    //  getch();
      refresh();
      endwin();

    return EXIT_SUCCESS;
}


/*Compilación --> gcc main.cpp interface.cpp -o interface -lncurses -lcurl || make*/
