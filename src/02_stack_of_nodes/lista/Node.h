#ifndef __STACK_H__
#define __STACK_H__

#include "List.h"
#include "Dom_string.h"
#define MAX_HIJOS 0x100

struct List;
struct DomString;

class Node {

	// NodeType
	static const unsigned short      ELEMENT_NODE = 1;
	static const unsigned short      ATTRIBUTE_NODE = 2;
	static const unsigned short      TEXT_NODE = 3;
	static const unsigned short      CDATA_SECTION_NODE = 4;
	static const unsigned short      ENTITY_REFERENCE_NODE = 5;
	static const unsigned short      ENTITY_NODE = 6;
	static const unsigned short      PROCESSING_INSTRUCTION_NODE = 7;
	static const unsigned short      COMMENT_NODE = 8;
	static const unsigned short      DOCUMENT_NODE = 9;
	static const unsigned short      DOCUMENT_TYPE_NODE = 10;
	static const unsigned short      DOCUMENT_FRAGMENT_NODE = 11;
	static const unsigned short      NOTATION_NODE = 12;

    public:
		DomString        *nodeName;
		DomString        *nodeValue;

		unsigned short   nodeType;
		Node             const *parentNode;
		List			 *childList;

		Node             const *firstChild;
		Node             const *lastChild;
		Node             const *previousSibling;
		Node             const *nextSibling;

        int id;
        Node *sgte;
        Node *prev;
        
        Node cloneNode(bool deep);
<<<<<<< HEAD
        Node insertBefore(Node *newChild, Node *refChild);	// raises(DOMException)
        Node remplaceChild(Node *newNode, Node* oldNode);	// raises(DOMException)
        bool hasChildNodes();
        Node removeChild(Node *oldChild);					// raises(DOMException)

		void             normalize();
		bool             isSupported(DomString *feature, DomString *version);
		DomString        const *namespaceURI;
		DomString        const *prefix;
		DomString        const *localName;
		bool			 hasAttributes();

		Node();
=======
        Node insertBefore(Node *newChild, Node *refChild);
        Node remplaceChild(Node *newNode, Node* oldNode);
        bool hasChildNodes();
		Node removeChild(Node *oldChild);
		Node appendChild(Node *newChild);
		void normalize();
		bool hasAttributes() const noexcept;
>>>>>>> subordinate
};


#endif

