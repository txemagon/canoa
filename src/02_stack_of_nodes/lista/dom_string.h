#ifndef __DOM_STRING_H__
#define __DOM_STRING_H__

using namespace std;

class DOMString {
	char* text;
public:
	DOMString(const char *initial_text = "");
	DOMString(char *initial_text);
};


#endif
