﻿#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include "stack.h"
#include "nodo.h"
using namespace std;

void menu () {
    //system("cls");
    printf("1.-Añadir\n");
    printf("2.-SacarDato\n");
    printf("3.-VerPila\n");
    printf("4.-LimpiarPila\n");
	printf("5.-BuscarNodo\n");
	printf("6.-Buscar num\n");
	printf("7.-Eliminar hijo\n");
	printf("8.-Test\n");
    printf("9.-Salir\n");
}

void mostrarPila(TPila *pila){
    TPila *temp = new TPila();
	int contador = 1;
    while(!pila->pilaVacia()){
		printf("/*---STACK---*/\n");
		printf("Posicion %i: %i\n",contador, pila->valorTope());
        temp->push(pila->valorTope());
        pila->pop();
		contador++;
    }
	while(!temp->pilaVacia()) {
		pila->push(temp->valorTope());
		temp->pop();
	}
	while(!temp->pilaVacia()) {
		pila->push(temp->valorTope());
		temp->pop();
	}
}


int main(){
	int opcion, id, num, sacado;
	char addChild;
    TPila *pila = new TPila();
	TPila *list = new TPila();
	Nodo *nodoS = new Nodo();
	Nodo *nod = new Nodo();
	Nodo *temp = new Nodo();
	Nodo *test = new Nodo();
    do{
		int data = 0;
        menu();
        scanf_s("%d", &opcion);
        switch(opcion){
            case 1:
                if(!pila->pilaLlena()){
					printf("Dato del nodo: ");
					scanf_s("%i", &data);
                    pila->push(data);
                    printf("Nodo añadido correctamente\n");
                }
                else
                  printf("La pila se encuentra llena");
                break;
            case 2:
                if(!pila->pilaVacia()){
                    pila->pop();
                    printf("Nodo sacado correctamente");
                }
                else
                  printf("La pila se encuentra vacia");
                break;
            case 3:
                mostrarPila(pila);
                break;
            case 4:
                pila->limpiarPila();
                break;
			case 5:
				//id = 0;
				//printf("Id del nodo: ");
				//scanf_s("%i", &id);
				//*nodoS = pila->searchById(id);
				//printf("El nodo buscado es: %i", nodoS->getDato());
				break;
			case 6:
				num = 0;
				//printf("Numero a buscar");
				//scanf_s("%i", &num);
				//printf("el numero esta en la pos: %i\n", pila->getposNodo(num));
				break;
			case 7:
				//sacado = 0;
				//printf("Indique el dato del nodo que quieres sacar");
				//scanf_s("%i", &sacado);
				//pila->searchById(sacado);
				//printf("Se quitado el hijo del nodo indicado");
				break;
			case 8:
				nod->setDato(02);
				temp->setDato(03);
				temp->setNextOne(NULL);

				nod->setChildNode(*temp);

				*list = nod->getChildList();
				printf("%i", list->getTope()->getDato());

				break;
        }
    }while(opcion != 9);

    //return EXIT_SUCCESS;
    return 0;
}
