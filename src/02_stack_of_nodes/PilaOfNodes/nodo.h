#ifndef __NODO_H__
#define __NODO_H__
#include <string>
#include <iostream>
#include <stdio.h>
#include "stack.h"

struct TPila;
struct Nodo;

const int cantidad_nodos = 10;
class Nodo{
    private:
        int dato;
	int numChilds;
        Nodo* next;
        TPila* childList;
    public:
        Nodo();
        void setDato(int dt);
        void setNextOne(Nodo* next);
        int getDato();
        Nodo* getNextOne();
		Nodo Nodo::setChildNode(Nodo newChild);
	TPila getChildList();
	bool hasChild();
        Nodo insertBefore(Nodo *newChild, Nodo *refChild);
        Nodo cloneNode(bool deep);

};
#endif
