#ifndef __STACK_H__
#define __STACK_H__
#include "nodo.h"
#include <iostream>
#include <string>
#include <stdio.h>
struct Nodo;
using namespace std;
class TPila{
    private:
        int totalNodos;
        Nodo* tope;
    public:
        TPila();
        int getTotalNodos();
        void setTotalNodos(int _totalNodos);
        Nodo* getTope();
        void setTope(Nodo _tope);
        bool pilaLlena();
        bool pilaVacia();
        void push(int dato);
        void pop();
        int valorTope();
        void limpiarPila();
	Nodo searchById(int id);
	Nodo searchByNode(Nodo *node);
	int getposNodo(Nodo *nodo);
	void remplaceNode(Nodo *old_node, Nodo new_node);
	void setNodeInPosition(int position, Nodo *node);
};
#endif

