#include <stdio.h>
#include <stdlib.h>
#include "List.h"
#include "Node.h"

#define MAX_HIJOS 0x100

List::List() {
	n_nodos = 0;
	ult_nodo = NULL;
}

void
List::printList(){
    Node *temp = new Node();
    temp = ult_nodo;
    while(temp->prev != NULL){
        printf("%i\n", temp->id);
        temp = temp->prev;
    }
}

void
List::insertNodeAtBegin(Node *inicio) {
	if (n_nodos == 0) {
		ult_nodo = inicio;
		n_nodos++;
	}
	else {
		Node *temp = new Node();
		temp = ult_nodo;
		while (temp->prev != NULL) {
			temp->id++;
			temp = temp->prev;
		}
		temp->prev = inicio;
		inicio->sgte = temp;
	}
}

void
List::insertNodeAtEnd(Node *new_node) {
    Node *temp = new Node();

	if (n_nodos == 0) {
		ult_nodo = new_node;
		n_nodos++;
	} else {
		ult_nodo->sgte = new_node;
		new_node->prev = ult_nodo;
		ult_nodo = new_node;
		n_nodos++;
	}
}

bool
List::searchNode(Node *node) {
    Node *current = new Node();
    current = ult_nodo;
    while(current != NULL){
        if(current == node)
            return true;
        current = current->sgte;
    }
    return false;
}
