#ifndef __STACK_H__
#define __STACK_H__

#include "List.h"
#define MAX_HIJOS 0x100

struct List;

class Node {
public:
	int id;
	Node *sgte;
	Node *prev;
        List *childList;

	Node();
        Node cloneNode(bool deep);
        Node insertBefore(Node *newChild, Node *refChild);
};


#endif

