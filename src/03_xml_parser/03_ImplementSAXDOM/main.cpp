#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <string>
#include <cstring>
//Parser Libs
#include <libxml/parser.h>
#include <libxml/xmlmemory.h>
#include <libxml/HTMLparser.h>
//Files Libs
#include "definitions.h"

#define DEBUG(x) printf(x)

#define MAX 0x100

//!class Node
//!cabecera de la clase Node
class Node;

stack *pila = new stack();      //Pila de graph
List *lista_general = new List();

using namespace std;

//!char *etiquetas_html[MAX]
//!array que contiene las etiquetas de HTML
char *etiquetas_html[MAX] = {"a", "abbr", "acronym", "address", "applet", "area", "b", "base", "basefont", "bdo", "big", "blockquote",
    "body", "br", "button", "caption", "center", "cite", "code", "col", "colgroup", "dd", "del", "dfn", "dir", "div", "dl", "dt", "em",
    "fieldset", "font", "form", "frame", "frameset", "h1", "h2", "h3", "h4", "h5", "h6", "head", "hr", "html", "i", "iframe", "img",
    "input", "ins", "isindex", "kbd", "label", "legend", "li", "link", "map", "menu", "meta", "noframes", "noscript", "object", "ol",
    "optgroup", "option", "p", "param", "pre", "q", "s", "samp", "script", "select", "small", "span", "strike", "strong", "style",
    "sub", "sup", "table", "tbody", "td", "textarea", "tfoot", "th", "thead", "title", "tr", "tt", "u", "ul", "var"};



/*!Excepciones del fichero
 *\param doc puntero al documento
 *\param cur nodo de la etiqueta HTML
 *\return void
 */
static void
file_exceptions(xmlDocPtr doc, xmlNodePtr cur){
    if(doc == NULL){fprintf(stderr, "File not valid");}
    if(cur == NULL){fprintf(stderr, "File empty");}
}

/*!Buscamos etiquetas hijo dentro de la etiqueta padre(cur)
 *!y las parseamos recursivamente
 *\param cur nodo de la etiqueta HTML
 *\return void
 */
static void
parse_child(xmlNodePtr cur){
    printf("parseando hijos de %s\n", cur->name);

    Node *node_lvl2 = new Node();           /*Creamos el nodo padre*/
    if(cur != NULL)
       node_lvl2 -> nodeName = (char *)cur->name;
    lista_general->insertNodeAtEnd(node_lvl2);                    /*Lo insertamos en la lista general*/

    cur = cur->xmlChildrenNode;         /*cur es ahora la etiqueta hijo*/

    List *lista_hijos = new List();

    node_lvl2->childList = lista_hijos;   /*Asociamos la lista al padre*/

    while(cur != NULL){                 /*Mientras siga habiendo etiquetas entra*/
        for (int i = 0; i<MAX; i++){    /*Comprobamos todas la etiquetas del array*/
            if((!xmlStrcmp(cur->name,(const xmlChar *) etiquetas_html[i]))){
                printf("parseando  etiqueta %s\n", cur->name);

                /*IMPLEMENTANDO LA CREACIÓN DEL NODO*/
                Node *node = new Node((char *)cur->name);           /*Creamos un nodo con nombre cur->name*/
                node->nodeType = 1;                                 /*El tipo del nodo es ELEMENTO*/
                node->nodeContent = (char *) xmlNodeGetContent(cur);/*Insertamos el contenido*/
                lista_hijos->insertNodeAtEnd(node);                 /*Lo insertamos en la lista hijos del padre*/

                parse_child(cur);       /*Usamos recursivadad para parsear los hijos de esta etiqueta*/
            }
        }
        cur = cur->next;                /*Pasa a la próxima etiqueta*/
    }

}

/*!Iniciamos el parseo del documento y llamamos a parse_child para
 *!parsear todas las etiquetas hijo también
 *\param docname nombre del fichero
 *\return void
 */
static void
parse_document(char *docname){

    xmlDocPtr doc;
    xmlNodePtr cur;

    DEBUG("Starting the parse\n");      /*para testSAX, de momento es = que printf*/

    file_exceptions(doc, cur);

//    doc = xmlParseFile(docname); //To XML
    doc = htmlReadFile(docname, NULL, HTML_PARSE_NOBLANKS | HTML_PARSE_NOERROR | HTML_PARSE_NOWARNING | HTML_PARSE_NONET);

    cur = xmlDocGetRootElement(doc);   /*Primer elemento del XML*/

    parse_child(cur);

    recorre_list(lista_general, pila);                          /*Encargado de sacar frames*/

    xmlFreeDoc(doc);
}

int main(int argc, char *argv[]){

    char *docname;

    if(argv[1] == NULL){
        fprintf(stderr, "No imput file\n");
        return EXIT_FAILURE;
    }

    docname = argv[1];                /*Pasamos el XML file por parámetro*/

    parse_document(docname);

    return EXIT_SUCCESS;
}



/*TO COMPILE: g++ main.cpp -I/usr/include/libxml2 -lxml2 -o main*/
