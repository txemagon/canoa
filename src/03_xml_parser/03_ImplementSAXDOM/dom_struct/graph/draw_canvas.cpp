#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <fstream>
#include "stack.h"
#include "../node.h"
#include "draw_canvas.h"
#include <unistd.h>

#define NOMBRE "frame.dot"

using namespace std;

struct Node;

string unionNodes(Node *node1, Node *node2) {
	string arrow = "->";
        string result = node1->nodeName + arrow + node2->nodeName;
	return result;
}

void generateGraph(stack *p) {

	ofstream file;
	file.open(NOMBRE);

	file << "digraph{\n";

	for (int cnt = 0; cnt<p->cima; cnt++){
            if(p->dato[cnt] != "")
		file << p->dato[cnt];
        }


	file << "}";

	file.close();
	system("./script.sh");
        sleep(1);
}
