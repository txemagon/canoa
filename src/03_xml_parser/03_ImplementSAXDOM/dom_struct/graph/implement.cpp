#include <stdio.h>
#include <stdlib.h>
//Files
#include "stack.h"
#include "draw_canvas.h"
#include "../node.h"
#include "../list.h"
#include "implement.h"

using namespace std;

class stack;
class List;
class Node;

/*!Función que usamos para implementar la recursividad en recorre_list
 *\param temp Node
 *\param pila stack
 *\return void
 */
void recorre_child_list(Node *temp, stack *pila) {
    if (temp->childList != NULL) {                      //Comprobamos si tiene una lista de hijos asociada
        Node *temp2 = new Node();
        temp2 = temp->childList->ult_nodo;
        while (temp2 != NULL) {
            pila->push(unionNodes(temp, temp2));
            generateGraph(pila);
            recorre_child_list(temp2, pila);                 //Usamos la recursividad para comprobar todos los nodos
            temp2 = temp2->prev;
        }
    }
    else{
//        std::string st (temp->nodeName);
//        pila->push(st + "\n");                  //Metemos en la pila el nodo sin referencia
    }
}

/*!Función encargada volcar los datos de la lista a la pila que posteriormente
 *!usaremos para crear el frame.dot
 *\param list List
 *\param pila stack
 *\return void
 */

void recorre_list(List *list, stack *pila) {
    Node *temp = new Node();
    temp = list->first_node;
    while (temp != NULL) {
        generateGraph(pila);
        recorre_child_list(temp, pila);
        temp = temp->sgte;
    }
}

