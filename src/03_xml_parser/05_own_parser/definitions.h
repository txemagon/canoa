#ifndef __DEFINITIONS_H__
#define __DEFINITIONS_H__
//Dom docs
#include "dom_struct/list.h"
#include "dom_struct/node.h"
//Graph docs
#include "dom_struct/graph/implement.h"
#include "dom_struct/graph/stack.h"
#include "dom_struct/graph/draw_canvas.h"
//Parser docs
#include "parser/parser.h"
#include "parser/legacy.h"

#endif
