#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "definitions.h"

void print_list_labels(List *ls){
    Node *temp = new Node();
    temp = ls->first_node;
    while(temp != NULL){

        cout << temp->label_name << endl;

        temp = temp->sgte;
    }

}

int main(int argc, char* argv[]){
    char* docname;
    //docname = argv[1];
    docname = "html_examples/mediumExample.html";

    List *total_list = new List();
    Node *root = new Node();

    start_parse_file(docname, total_list);

    print_list_labels(total_list);

    return EXIT_SUCCESS;
}
