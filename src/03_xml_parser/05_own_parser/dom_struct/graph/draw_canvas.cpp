#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <fstream>
#include "stack.h"
#include "draw_canvas.h"
#include "../node.h"
#include <unistd.h>

#define NOMBRE "frame.dot"

using namespace std;

struct Node;

string unionNodes(Node *node1, Node *node2) {
    char dir1[20];
    char dir2[20];
    sprintf(dir1,"%p",node1->dir_memory);
    sprintf(dir2,"%p",node2->dir_memory);

    string arrow = "->";
    string result = "";
        result += node1->nodeName;
        result += dir1;
        result += arrow;
        result += node2->nodeName;
        result += dir2;
        result += "\n";

    return result;
}

void generateGraph(stack *p) {

    ofstream file;
    file.open(NOMBRE);

    file << "digraph{\n";

    for (int cnt = 0; cnt<p->cima; cnt++)
        file << p->dato[cnt];


    file << "}";

    file.close();
    system("./script.sh");
    sleep(2);
}
