#ifndef __LIST_H__
#define __LIST_H__

#include <stdio.h>
#include <string>
#include "node.h"

struct Node;

using namespace std;

class List
{
public:
	int n_nodos;
	Node *ult_nodo;
        Node *first_node;

	List();
        void printList();
	void insertNodeAtBegin(Node *inicio);
        void insertNodeAtEnd(Node *new_node);
        bool searchNode(Node *node);
        bool searchNodeByTypeAndName(string type,string label);

};

#endif
