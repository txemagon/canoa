#include <stdio.h>
#include <stdlib.h>
//Own docs
#include "definitions.h"

/*Cuando acaba de crear una lista de nodos estandar, crea las herencias con estas 2 funciones usando las etiquetas de cierre y apertura
 * check_child_label se usa para implementar la recursividad de forma que hasta que mientras no encuentra una etiqueta de cierre
 * de esa etiqueta en concreto va metiendo las etiquetas hijo en la lista de hijos*/
string close = "close";

Node create_child_struct(List *ls){
    Node *root = new Node();
    root = ls->first_node;

    check_child_label(root, root->sgte);

    return *root;
}

Node *new_node = new Node();

Node check_child_label(Node *root, Node *temp){
    while(temp != NULL){
     //   if(not_close_label(temp->label_name))
     //       return *temp;
        if(temp->label_name == root->label_name && temp->label_type == close)
            return *temp;
        else {
            *new_node = check_child_label(temp, temp->sgte);
            Node *other = new Node();
            other->clone_node(temp);
            other->sgte = NULL;
            other->parentNode = root;
            root->insert_child_at_end(other);

            temp = new_node->sgte;
        }
    }
    return *temp;
}

bool not_close_label(string label){
    string unclose_labels[5] = {"P"};
    for(int cnt=0; cnt<5; cnt++)
        if(unclose_labels[cnt] == label)
            return true;
    return false;
}
