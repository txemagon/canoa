#ifndef __PARSER_H__
#define __PARSER_H__

struct List;
struct Node;

void insert_label(List *general_labels);
void colect_caracter(char caracter);
void rating_caracter(char caracter,List *general_labels);
void start_parse_file(char *docname, List *general_labels);
void parse_file(List *ls, FILE *pf);
void print_events_on_file(List *general_labels);
Node search_node_father(List *labels_list);


#endif
