#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <cstring>
//Own documents
#include "definitions.h"

using namespace std;

bool open_label = false;
bool inside_label = false;    //<html inside_label>
bool on_label = false;        //<html> On label </html>

bool is_a_comment = false;
bool is_attribute = false;

string general_content;
string label;
string father_label;
string attribute;

char prev_char;

void start_parse_file(char *docname, List *labels_list){
    FILE *fp;
    int option;
    fp = fopen(docname, "r");

    char ch;
    while ((ch = fgetc(fp)) != EOF) {
        rating_caracter(ch, labels_list);
        prev_char = ch;
    }
    fclose(fp);
}

void rating_caracter(char caracter,List *general_labels){

    if(on_label == true)
        father_label = label;

    if(caracter == '<'){
        inside_label = true;
        open_label = true;
        label = "";
        return;
    }
    if(caracter == '>'){
        insert_label(general_labels);
        inside_label = false;
        is_attribute = false;
        return;
    }
    if(open_label == true && caracter == '/'){
        open_label = false;
        return;
    }
    if(prev_char == '!' && caracter == '-'){
        is_a_comment = true;
    }
    if(caracter == ' ' && inside_label == true){
        is_attribute = true;
    }
    else {
        colect_caracter(caracter);
        return;
    }
}

void colect_caracter(char caracter){
    if(is_attribute == true){
        attribute += caracter;
        return;
    }if(inside_label == true)
        label += caracter;
   else
        general_content += caracter;
}

void insert_label(List *general_labels){
    Node *node = new Node();

    node->label_name = label;

    if(general_content != ""){
        node->nodeContent = general_content;
        general_content = "";
    }

    if(open_label == true)
        node->label_type = "open";
    else
        node->label_type = "close";

    general_labels->insertNodeAtEnd(node);
    open_label = false;
}

void insert_label_child(List *labels_list){
    Node *node_father = new Node();
    *node_father = search_node_father(labels_list);
    if(node_father != NULL){
        List *child_list = new List();
        insert_label(child_list);
        node_father->childList = child_list;
    }
}

Node search_node_father(List *labels_list){
    Node *temp = new Node();
    temp = labels_list->ult_nodo;
    while(temp != NULL){
        if(temp->label_name == father_label && temp->label_type == "open")
            return *temp;

        temp = temp->prev;
    }
    return NULL;
}



/*Funcion que imprime eventos de las etiquetas en un txt*/
/*
   void print_events_on_file(List *general_labels){
   Label *temp = new Label();
   std::ofstream outfile("events.txt");

   if(general_labels->ult_label == NULL){
   printf("error");
   return;
   }

   temp = general_labels->ult_label;
   while(temp != NULL){

   outfile << temp->content + " - " + temp->type << std::endl;
//system(date +%T:%N | cut -b-11)
temp = temp->prev;
}

outfile.close();
}
*/
