#include <string>
#ifndef __LEGACY_H__
#define __LEGACY_H__

Node create_child_struct(List *ls);
Node check_child_label(Node *root, Node *temp);
bool is_closed();
bool not_close_label(std::string label);

#endif
