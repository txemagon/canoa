#ifndef __PARSER_H__
#define __PARSER_H__

struct Labels_list;

void insert_label(Labels_list *general_labels);
void colect_caracter(char caracter);
void rating_caracter(char caracter,Labels_list *general_labels);
void start_parse_file(char *docname, Labels_list *general_labels);
void parse_file(Labels_list *ls, FILE *pf);
void print_events_on_file(Labels_list *general_labels);


#endif
