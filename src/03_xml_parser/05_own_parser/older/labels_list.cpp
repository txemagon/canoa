#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "labels_list.h"
#include "label.h"

using namespace std;

struct Label;

Labels_list::Labels_list()
{
    n_labels = 0;
    ult_label = NULL;
    first_label = NULL;
};

void
Labels_list::insert_label_at_end(Label *label) {
    Label *temp = new Label();
    if (this == NULL)		//Error en caso de no tener asociada lista
        return;

    if (n_labels == 0) {
        ult_label = label;
        first_label = label;
        n_labels++;
    } else {
        ult_label->sgte = label;
        label->prev = ult_label;
        ult_label = label;
        n_labels++;
    }
}

bool
Labels_list::search_label(string type, string label){
    Label *temp = new Label();
    temp = this->ult_label;
    while(temp != NULL){
        if(temp->name == label && temp->type == type)
            return true;

        temp = temp->prev;
    }
    return false;
}
