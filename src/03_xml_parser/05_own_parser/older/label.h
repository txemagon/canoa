#ifndef __LABEL_H__
#define __LABEL_H__
#include <string>

using namespace std;

class Label {
    public:
        //Data
        string type;
        string label;
        string name;
        string content;

        //Pointers
        Label *sgte;
        Label *prev;

        //Contructor
        Label();
        Label(string type, string name);
};

#endif
