#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <fstream>
//Own documents
#include "label.h"
#include "labels_list.h"
#include "parser.h"

using namespace std;

//Define structs
struct Labels_list;
struct Label;

bool open_label = false;
bool inside_label = false;
bool on_label = false;

string general_content;
string label;
string label_content;

void insert_label(Labels_list *general_labels){
    Label *lb;

    if(open_label == true)
        lb = new Label("open", label);
    else
        lb = new Label("close", label);

    open_label = false;
    general_labels->insert_label_at_end(lb);
}

void colect_caracter(char caracter){
    if(inside_label == true)
        label += caracter;
    else {
        general_content += caracter;
    }

}

void rating_caracter(char caracter,Labels_list *general_labels){

    on_label = general_labels->search_label("close", label);

    if(caracter == '<'){
        inside_label = true;
        open_label = true;
        label = "";
        return;
    }
    if(caracter == '>'){
        if(on_label == true){


        }else{
            insert_label(general_labels);
            inside_label = false;
        }
        return;
    }
    if(open_label == true && caracter == '/'){
        open_label = false;
        return;
    }
    else {
        colect_caracter(caracter);
        return;
    }
}

void start_parse_file(char *docname, Labels_list *general_labels){
    FILE *filePointer;
    int option;
    filePointer = fopen(docname, "r");

    parse_file(general_labels, filePointer);

    fclose(filePointer);
    print_events_on_file(general_labels);
}

void parse_file(Labels_list *ls, FILE *fp){
    char ch;
    while ((ch = fgetc(fp)) != EOF) {
        rating_caracter(ch, ls);
    }
}

void parse_labels(Labels_list *general_labels){
    Label *temp = new Label();
}

void print_events_on_file(Labels_list *general_labels){
    Label *temp = new Label();
    std::ofstream outfile("events.txt");

    if(general_labels->ult_label == NULL){
        printf("error");
        return;
    }

    temp = general_labels->ult_label;
    while(temp != NULL){

        outfile << temp->content + " - " + temp->type << std::endl;
        //system(date +%T:%N | cut -b-11)
        temp = temp->prev;
    }

    outfile.close();
}

