#include <stdio.h>
#include <stdlib.h>
#include <string>

#include "label.h"

using namespace std;

Label::Label(){
    type = "";
    content = "";
    sgte = NULL;
    prev = NULL;
}

Label::Label(string type, string name){
    this->type = type;
    this->name = name;
    sgte = NULL;
    prev = NULL;
}
