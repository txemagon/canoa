#ifndef __LABLES_LIST_H__
#define __LABELS_LIST_H__

#include <stdio.h>
#include <stdlib.h>
#include <string>

struct Label;

using namespace std;

class Labels_list {
    public:
    int n_labels;
    Label *ult_label;
    Label *first_label;

    Labels_list();
    void insert_label_at_end(Label *label);
    bool search_label(string type, string label);
};

#endif
