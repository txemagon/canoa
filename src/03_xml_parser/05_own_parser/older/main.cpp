#include <stdio.h>
#include <stdlib.h>
#include "parser.h"
#include "labels_list.h"

int main(int argc, char* argv[]){
    char * docname;
    docname = argv[1];
    Labels_list *ls = new Labels_list();

    start_parse_file(docname, ls);

    return EXIT_SUCCESS;
}
