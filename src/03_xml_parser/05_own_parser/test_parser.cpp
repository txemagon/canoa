#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "definitions.h"

int num = 0;
string anotation = "";

void print_anotation(){
    anotation = "";
    for(int cnt=0; cnt<=num; cnt++){
        anotation += "--";
    }
    anotation += ">";

}

void read_childs(Node *temp){
    num += 1;
    while(temp != NULL){
        print_anotation();
        std::cout << anotation + temp->label_name << endl;

        if(temp->childList != NULL){
            read_childs(temp->childList->first_node);
        }
        temp = temp->sgte;
    }
    num += -1;
}

void recursive_read(Node *root){

    std::cout << root->label_name << endl;

    if(root->childList != NULL)
        read_childs(root->childList->first_node);
}

int main(int argc, char* argv[]){
    char* docname;
    //docname = argv[1];
    docname = "html_examples/mediumExample.html";

    List *total_list = new List();
    Node *root = new Node();

    start_parse_file(docname, total_list);
    *root = create_child_struct(total_list);

    recursive_read(root);

    return EXIT_SUCCESS;
}
