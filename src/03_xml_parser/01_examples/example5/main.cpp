#include <stdio.h>
#include <stdlib.h>
#include <libxml/xmlreader.h>

void process_node(xmlTextReaderPtr reader){
    xmlChar *name, *value;

    name = xmlTextReaderName(reader);
    if(name == NULL)
        name = xmlStrdup(BAD_CAST "--");
    value = xmlTextReaderValue(reader);

    printf("%d %d %s %d", xmlTextReaderDepth(reader),
            xmlTextReaderNodeType(reader),
            name,
            xmlTextReaderIsEmptyElement(reader));
    xmlFree(name);
    if(value == NULL)
        printf("\n");
    else {
        printf("%s\n", value);
        xmlFree(value);
    }

}

void stream_file(char *filename){
    xmlTextReaderPtr reader;
    int ret;

    reader = xmlNewTextReaderFilename(filename);
    if(reader != NULL){
       while(ret == 1){
          process_node(reader);
          ret = xmlTextReaderRead(reader);
      }

       xmlFreeTextReader(reader);
    } else {
        printf("Unable to open %s\n", filename);
    }
    xmlFreeTextReader(reader);
}

int main(){
    char * filename = "file.xml";
    stream_file(filename);


    return EXIT_SUCCESS;
}
