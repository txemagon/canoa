#include <libxml/parser.h>
#include <libxml/xpath.h>

int
main(int argc, char **argv) {

	xmlDocPtr doc;
	xmlNodePtr cur;

	docname = argv[1]

	doc = xmlParserFile(docname);

	if (doc == NULL) {
		fprintf(stderr, "document not parsed succesfully. \n");
		return (1);
	}

	cur = xmlDocGetRootElement(doc);

	if (cur == NULL) {
		fprintf(stderr, "empty document\n");
		xmlFreeDoc(doc);
		return (1);
	}

	if(xmlStrcmp(cur->name, (const xmlChar *) "story")) {
		fprintf(stderr, "document of the wrong type, root node != story");
		xmlFreeDoc(doc);
		return (1);
	}

	return (1);
}



/*
library(cosmicexplorer/xmlr)

  f = system.file("exampleData", "catalog.xml",  package = "XML")
  doc = xmlInternalTreeParse(f)
  docName(doc)

  doc = xmlInternalTreeParse("<a><b></b></a>", asText = TRUE)
      # an NA
  docName(doc)
  docName(doc) = "Simple XML example"
  docName(doc)
*/
