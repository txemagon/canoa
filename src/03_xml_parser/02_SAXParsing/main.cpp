#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <cstring>
#include <libxml/parser.h>
#include <libxml/xmlmemory.h>
#include <string>

#define DEBUG(x) printf(x)

#define MAX 0x100

using namespace std;

char *etiquetas_html[MAX] = {"a", "abbr", "acronym", "address", "applet", "area", "b", "base", "basefont", "bdo", "big", "blockquote",
    "body", "br", "button", "caption", "center", "cite", "code", "col", "colgroup", "dd", "del", "dfn", "dir", "div", "dl", "dt", "em",
    "fieldset", "font", "form", "frame", "frameset", "h1", "h2", "h3", "h4", "h5", "h6", "head", "hr", "html", "i", "iframe", "img",
    "input", "ins", "isindex", "kbd", "label", "legend", "li", "link", "map", "menu", "meta", "noframes", "noscript", "object", "ol",
    "optgroup", "option", "p", "param", "pre", "q", "s", "samp", "script", "select", "small", "span", "strike", "strong", "style",
    "sub", "sup", "table", "tbody", "td", "textarea", "tfoot", "th", "thead", "title", "tr", "tt", "u", "ul", "var"};



/*!Excepciones del fichero
 *\param doc
 *\param cur
 *\return void
 */
static void
file_exceptions(xmlDocPtr doc, xmlNodePtr cur){
    if(doc == NULL){fprintf(stderr, "File not valid");}
    if(cur == NULL){fprintf(stderr, "File empty");}
}


/*!Buscamos etiquetas hijo dentro de la etiqueta padre(cur)
 *!y las parseamos recursivamente
 *\param cur
 *\return void
 */
static void
parse_child(xmlNodePtr cur){
    printf("parseando hijos de %s\n", cur->name);
    cur = cur->xmlChildrenNode;         /*cur es ahora la etiqueta hijo*/

    while(cur != NULL){                 /*Mientras siga habiendo etiquetas entra*/
        for (int i = 0; i<MAX; i++){    /*Comprobamos todas la etiquetas del array*/
            if((!xmlStrcmp(cur->name,(const xmlChar *) etiquetas_html[i]))){
                //En la etiqueta HTML
                printf("parseando  etiqueta %s\n", cur->name);
                parse_child(cur);       /*Usamos recursivadad para parsear los hijos de esta etiqueta*/
            }
        }
        cur = cur->next;                /*Pasa a la próxima etiqueta*/
    }

}

/*!Iniciamos el parseo del documento y llamamos a parse_child para
 *!parsear todas las etiquetas hijo también
 *\param docname
 *\return void
 */
static void
parse_document(char *docname){

    xmlDocPtr doc;
    xmlNodePtr cur;

    DEBUG("Starting the parse\n");      /*para testSAX, de momento es = que printf*/

    file_exceptions(doc, cur);

    doc = xmlParseFile(docname);

    cur = xmlDocGetRootElement(doc);   /*Primer elemento del XML*/

    parse_child(cur);

    xmlFreeDoc(doc);
}

int main(int argc, char *argv[]){

    char *docname;

    if(argv[1] == NULL){
        fprintf(stderr, "No imput file\n");
        return EXIT_FAILURE;
    }

    docname = argv[1];                /*Pasamos el XML file por parámetro*/

    parse_document(docname);

    return EXIT_SUCCESS;
}



/*TO COMPILE: g++ main.cpp -I/usr/include/libxml2 -lxml2 -o main*/
