#include <stdio.h>
#include <stdlib.h>

#include "definitions.h"
#include "styles.h"

void render_a (Node *node) {
    printf("\e]8;;https://as.com\n\e\\%s\e]8;;\e\\", node->nodeContent); //de prueba dejo que solo se cambie el nombre
    printf("\n");
}

void render_abbr (Node *node) {

}

void render_b (Node *node) {
    //    printf(BLACK "%s" RESET, node->nodeContent);
    printf("\e[1m%s\e[0m\n", node->nodeContent);
}

//void render_dbo(Node *node){

//}

void render_big (Node *node) {
    printf("%s", node->nodeContent);

}

void render_button(Node *node){
    //Crear una caja
    printf("[%s]", node->nodeContent);
}

void render_cite(Node *node){
    //Poner en cursiva
    printf("%s", node->nodeContent);
}

void render_code(Node *node){
    //Poner letra código
    printf("%s", node->nodeContent);

}

void render_dfn(Node *node){
    //Poner en cursiva
    printf("%s", node->nodeContent);

}

void render_em(Node *node){
    //cursiva + negrita
    printf("%s", node->nodeContent);

}

void render_i (Node *node) {
    printf("\e[3m%s\e[0m\n", node->nodeContent);
}

void render_img(Node *node){
    //Mostramos como imagen.png
    //hacer comando que al poner "show imagen.png" abra imagen con script
    printf("%s", node->nodeContent);

}

void render_input(Node *node){
    printf("[_      ]");
    //hacer comando que al poner "insert 'texto' into input1"
}

void render_kdb(Node *node){
    //Poner letra de teclado, tipo distinto de letra
    printf("%s", node->nodeContent);

}

void render_label(Node *node){
    printf("%s ( )", node->nodeContent); //void
    printf("%s (*)", node->nodeContent); //checked
}

void render_q(Node *node){  //entre Comillas
    printf("\"%s\"", node->nodeContent);
}

void render_select(Node *node){
    //idk
    printf("%s", node->nodeContent);

}

void render_small(Node *node){
    printf("%s", node->nodeContent);
}

void render_span(Node *node){
    //idk
    printf("%s", node->nodeContent);

}

void render_strong (Node *node) {
    printf("\e[1m%s\e[0m\n", node->nodeContent);
}

void render_textarea (Node *node){
    printf("/-------------------------/\n");
    printf("%s\n", node->nodeContent);
    printf("/-------------------------/\n");
}

void render_var (Node *node){
    //Cursiva
    printf("%s\n", node->nodeContent);
}


