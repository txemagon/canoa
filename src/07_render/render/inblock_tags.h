#ifndef __INBLOCK_TAGS_H__
#define __INBLOCK_TAGS_H__

class Node;

void render_dl(Node *node);
void render_dd(Node *node);
void render_dt(Node *node);
void render_div(Node *node);

#endif
