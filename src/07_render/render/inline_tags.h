#ifndef __INLINE_TAGS_H__
#define __INLINE_TAGS_H__

class Node;

void render_a (Node *node);
void render_abbr (Node *node);
void render_b (Node *node);
//void render_bdo(Node *node);
void render_big (Node *node);
void render_button(Node *node);
void render_cite (Node *node);
void render_code (Node *node);
void render_dfn(Node *node);
void render_em(Node *node);
void render_i (Node *node);
void render_img(Node *node);
void render_input(Node *node);
void render_kdb (Node *node);
void render_label(Node *node);
void render_q(Node *node);
void render_select(Node *node);
void render_small (Node *node);
void render_span(Node *node);
void render_strong (Node *node);
void render_textarea(Node *node);
void render_var (Node *node);

/*UNCLOSE*/
//br

#endif
