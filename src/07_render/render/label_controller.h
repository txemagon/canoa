#ifndef __LABEL_CONTROLLER_H__
#define __LABEL_CONTROLLER_H__

struct Node;
struct List;

void render_list(List *ls);
void render_root(Node *root);
bool check_type(char *label, int type);
void element_node(Node *node);
void inline_lb(Node *node);
void inblock_lb(Node *node);

#endif
