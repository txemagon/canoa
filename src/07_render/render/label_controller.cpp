#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <string.h>
//Extern files
#include "definitions.h"

#define INLINE 28
#define INBLOCK 33

struct Node;

//!Declaro todos los tipos de etiqueta inline
char *inline_labels[100] = {
    "b","big","i","small","abbr","acronym","cite","code","em","kbd","strong","samp","time","var",
    "a","bdo","br","img","map","q","script","span","sub","sup","button","input","label","select","textarea"
};

//!Declaro todos los tipos de etiquetas inblock
char *inblock_labels[100] = {
    "address","blockquote","center","dir","div","dl","fieldset","form","h1","h2","h3","h4","h5","h6","hr",
    "insindex","menu","noframes","noscript","ol","p","pre","table","ul",
    "dd","dt","frameset","li","tbody","td","tfoot","th","thread","tr"
};


//!Para hacer switch con char*

constexpr unsigned int str2int(const char* str, int h = 0)
{
    return !str[h] ? 5381 : (str2int(str, h+1) * 33) ^ str[h];
}


/*! This function render a list of nodes
 *\param list: lista
 *\return void
 * */
void render_list(List *ls){
    Node *temp = new Node();
    temp = ls->ult_nodo;
    while(temp != NULL){
        element_node(temp);
        printf("\n");

        temp = temp->prev;
    }
}

/*!Render a node root with child lists
 *
 * */
void render_root(Node *root){
    element_node(root);
}



//!Funcion que distingue entre una etiqueta inline o inblock
/*\param node: nodo etiqueta
 *\return void
 */
void element_node(Node *node){
    if(check_type(node->nodeName, 0) && node->nodeName != NULL){
        //Inline
        inline_lb(node);
    }else if(check_type(node->nodeName, 1) && node->nodeName != NULL){
        //Inblock
        inblock_lb(node);
    }else{
        //Default
    }
}



//!Funcion que compara todas las etiquetas
/*
   \param label: Nombre de la etiqueta a comparar
   \param type: 0 tipo inline 1 tipo inblock
   \return bool
   */
bool check_type(char *label, int type){
    bool result = false;
    if(type == 0){
        for(int cnt = 0; cnt < INLINE; cnt++){
            if(strcmp(inline_labels[cnt], label) == 0)
                result = true;
        }
    }else if(type == 1){
        for(int cnt = 0; cnt < INBLOCK; cnt++){
            if(strcmp(inblock_labels[cnt], label) == 0)
                result = true;
        }
    }
    return result;
}

//!Funcion que trata las etiqueta inline
/*\param node: nodo etiqueta
 *\return void
 */
void inline_lb(Node *node){
    //Tratamos las etiquetas inline
    switch(str2int(node->nodeName)){
        case str2int("a"):
            render_a(node);
            break;
        case str2int("abbr"):
            render_abbr(node);
            break;
        case str2int("big"):
            render_big(node);
            break;
        case str2int("button"):
            render_button(node);
            break;
        case str2int("cite"):
            render_cite(node);
            break;
        case str2int("code"):
            render_code(node);
            break;
        case str2int("dfn"):
            render_dfn(node);
            break;
        case str2int("em"):
            render_em(node);
            break;
        case str2int("i"):
            render_i(node);
            break;
        case str2int("img"):
            render_img(node);
            break;
        case str2int("input"):
            render_input(node);
            break;
        case str2int("kdb"):
            render_kdb(node);
            break;
        case str2int("label"):
            render_label(node);
            break;
        case str2int("q"):
            render_q(node);
            break;
        case str2int("select"):
            render_select(node);
            break;
        case str2int("small"):
            render_small(node);
            break;
        case str2int("span"):
            render_span(node);
            break;
        case str2int("strong"):
            render_strong(node);
            break;
        case str2int("textarea"):
            render_textarea(node);
            break;
        case str2int("var"):
            render_var(node);
            break;
    }
}

//!Funcion que trata las etiquetas inblock
/*\param node: nodo etiqueta
 *\return void
 */
void inblock_lb(Node *node){
    //Tratamos las etiquetas inblock
    switch(str2int(node->nodeName)){
        case str2int("address"):
            break;
        case str2int("center"):
            break;
        case str2int("dd"):
            break;
        case str2int("dt"):
            break;
        case str2int("dl"):
            break;
    }
}

