#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <string.h>

#include "definitions.h"

using namespace std;


int main(){

    char *inline_labels[29] = {
        "b","big","i","small","abbr","acronym","cite","code","em","kbd","strong","samp","time","var",
        "a","bdo","br","img","map","q","script","span","sub","sup","button","input","label","select","textarea"
    };

    List *main_list = new List();

    for(int cnt=0; cnt<29; cnt++){
        Node *node = new Node();
        node->nodeContent = inline_labels[cnt];
        node->nodeName = inline_labels[cnt];
        main_list->insertNodeAtEnd(node);
    }


    render_list(main_list);

    return EXIT_SUCCESS;
}
