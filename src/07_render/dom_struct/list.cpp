#include <stdio.h>
#include <stdlib.h>
#include "list.h"
#include "node.h"

#define MAX_HIJOS 0x100

List::List() {
    n_nodos = 0;
    ult_nodo = NULL;
    first_node = NULL;
}

/**
 *
 */
void
List::printList(){
    int cnt = 0;
    Node *temp = new Node();
    temp = ult_nodo;
    cnt = this->n_nodos -1;

    while (cnt > 0) {
        temp = temp->prev;
        cnt--;
    }
}


 //! Inserta un Nodo al principio de la lista
 /*!
   \param inicio Node
   \return void
  */
void
List::insertNodeAtBegin(Node *inicio) {
    if (n_nodos == 0) {
        ult_nodo = inicio;
        first_node = inicio;
        n_nodos++;
    }
    else {
        Node *temp = new Node();
        temp = ult_nodo;
        while (temp->prev != NULL) {
            temp = temp->prev;
            n_nodos++;
        }
        temp->prev = inicio;
        inicio->sgte = temp;
        n_nodos++;
    }
}
//! Inserta un nodo al final de la lista
 /*
  \param new_node Node
  \return void
 */
void
List::insertNodeAtEnd(Node *new_node) {
    Node *temp = new Node();
    if (this == NULL)		//Error en caso de no tener asociada lista
        return;

    if (this->n_nodos == 0) {
        ult_nodo = new_node;
        first_node = new_node;
        n_nodos++;
    } else {
        ult_nodo->sgte = new_node;
        new_node->prev = ult_nodo;
        ult_nodo = new_node;
        n_nodos++;
    }
}

//!Busca un nodo en la lista
/*
 \param n
*/
bool
List::searchNode(Node *node) {
    Node *current = new Node();
    current = ult_nodo;
    while(current != NULL){
        if(current == node)
            return true;
        current = current->prev;
    }
    return false;
}

