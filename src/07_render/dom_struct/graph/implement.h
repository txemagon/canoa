#ifndef __IMPLEMENT_H__
#define __IMPLEMENT_H__
#include <string>

using namespace std;

class Node;
class stack;
class List;

void recorre_child_list(Node *temp, stack *pila);
void recorre_list(List *list, stack *pila);


#endif
