#ifndef __STACK_H__
#define __STACK_H__

#include <string>
#include "list.h"
#define MAX_HIJOS 0x100

struct List;
struct DomString;

class Node {

    public:
    std::string      content;
    std::string      tag_type;
    std::string      tag_name;
    std::string      attribute;

    Node             *parent_node;
    List	     *child_list;

    Node             const *first_child;
    Node             const *last_child;
    bool             is_meltdown = false;

    int id;
    Node *sgte;
    Node *prev;
    char dir_memory[20];

    Node clone_node(bool deep);
    Node insert_before(Node *new_child, Node *ref_child);	// raises(DOMException)
    Node remplace_child(Node *new_node, Node* old_node);	// raises(DOMException)
    bool has_child_nodes();
    Node remove_child(Node *old_child);					// raises(DOMException)

    bool			 has_attributes();

    Node();
    Node(std::string tag_name);

    void insert_child_at_end(Node *child_node);
    void clone_node(Node *new_node);
};


#endif

