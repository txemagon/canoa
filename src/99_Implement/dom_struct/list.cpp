#include <stdio.h>
#include <string>
#include <stdlib.h>
#include <iostream>
#include "list.h"
#include "node.h"

using namespace std;

#define MAX_HIJOS 0x100

List::List() {
    n_nodos = 0;
    ult_nodo = NULL;
    first_node = NULL;
}

/**
 *
 */
void
List::print_list(){
    int cnt = 0;
    Node *temp = new Node();
    temp = ult_nodo;
    cnt = this->n_nodos -1;

    while (cnt > 0) {
        cout << temp->tag_name << endl;
        temp = temp->prev;
        cnt--;
    }
}


//! Inserta un Nodo al principio de la lista
/*!
  \param inicio Node
  \return void
  */
void
List::insert_node_at_begin(Node *inicio) {
    if (n_nodos == 0) {
        ult_nodo = inicio;
        first_node = inicio;
        n_nodos++;
    }
    else {
        Node *temp = new Node();
        sprintf(temp->dir_memory,"%p",temp);
        temp = ult_nodo;
        while (temp->prev != NULL) {
            temp = temp->prev;
            n_nodos++;
        }
        temp->prev = inicio;
        inicio->sgte = temp;
        n_nodos++;
    }
}
//! Inserta un nodo al final de la lista
/*
   \param new_node Node
   \return void
   */
void
List::insert_node_at_end(Node *new_node) {
    //   sprintf(new_node->dir_memory,"%p", new_node);
    if (this == NULL)		//Error en caso de no tener asociada lista
        return;

    if (this->n_nodos == 0) {
        ult_nodo = new_node;
        first_node = new_node;
        n_nodos++;
    } else {
        ult_nodo->sgte = new_node;
        new_node->prev = ult_nodo;
        ult_nodo = new_node;
        n_nodos++;
    }
}

//!Busca un nodo en la lista
/*
   \param n
   */
bool
List::search_node(Node *node) {
    Node *current = new Node();
    current = ult_nodo;
    while(current != NULL){
        if(current == node)
            return true;
        current = current->prev;
    }
    return false;
}

void
List::remove_node(Node *old_node){
    Node *previo = new Node();
    Node *siguiente = new Node();

    previo = old_node->prev;
    siguiente = old_node->sgte;
    if (previo != NULL)
        siguiente->prev = previo;

    if (siguiente != NULL)
        previo->sgte = siguiente;

    this->n_nodos--;
}

bool
List::search_node_by_type_and_name(string type,string tag){
    Node *temp = new Node();
    temp = this->ult_nodo;
    while(temp != NULL){
        if(temp->tag_name == tag && temp->tag_type == type)
            return true;

        temp = temp->prev;
    }
    return false;
}

/*
Node
List::get_node_by_type_and_name(string type, string tag){
    Node *temp = new Node();
    temp = this->ult_nodo;
    while(temp != NULL){
        if(temp->tag_name == tag && temp->tag_type == type)
            return *temp;

        temp = temp->prev;
    }
    return void;
}

*/
