#include <stdio.h>
#include <stdlib.h>
#include "node.h"
#include "list.h"
#define MAX_HIJOS 0x100

struct DomString;

Node::Node() {
    id = 0;
    sgte = NULL;
    prev = NULL;
    child_list = NULL;
}
Node::Node(std::string tag_name) {
    this->tag_name = tag_name;
}

void
Node::clone_node(Node *new_node){
    this->content = new_node->content;
    this->tag_name = new_node->tag_name;
    this->tag_type = new_node->tag_type;

    this->sgte = new_node->sgte;
    this->prev = new_node->prev;

    this->child_list = new_node->child_list;
    this->id = new_node->id;
    this->attribute = new_node->attribute;

}

/*CHILD FUNCTIONS*/
Node
Node::insert_before(Node *new_child, Node *ref_child){
    if (child_list != NULL) {
        Node *temp = new Node();
        temp = child_list->ult_nodo;
        if (child_list->search_node(ref_child)) {  //Si refChild está en la lista
            new_child->prev = ref_child->prev;		//Previo del refChild es ahora el del newChild
            ref_child->prev = new_child;				//Previo del refChild es la direccion del newChild
            new_child->sgte = ref_child;				//Siguiente del newChild es la direccion del refChild
            child_list->n_nodos++;
        }
        else {
            //Insertamos el nodo al final de la lista hijos
            child_list->insert_node_at_end(new_child);
        }
    }
    return *new_child;
}

void
Node::insert_child_at_end(Node *child_node){
    if(child_list == NULL){
        List *child_list = new List();
        this->child_list = child_list;
    }
    this->child_list->insert_node_at_end(child_node);
}

Node
Node::remplace_child(Node *new_node, Node *old_node) {
    Node *current = new Node();
    current = this->child_list->ult_nodo;	//nodo auxiliar es el ultimo nodo de la lista hijos
    if (this->child_list->search_node(old_node)) {
        while (current != NULL) {			//Recorremos la lista actualizando el nodo auxiliar(current)
            if (current == old_node) {		//Lo encontramos y lo remplazamos por el nuevo
                if (old_node->prev != NULL) {			//Si el nodo tiene previo entra
                    Node *temp = new Node();
                    temp = old_node->prev;
                    new_node->prev = old_node->prev;
                    temp->sgte = new_node;
                }

                if (old_node->sgte != NULL) {			//Si el nodo tiene siguiente entra
                    Node *temp = new Node();
                    temp = old_node->sgte;
                    new_node->sgte = old_node->sgte;
                    temp->prev = new_node;
                }
                if (old_node == this->child_list->ult_nodo) {		//En caso de ser el ultimo nodo debemos actualizar el puntero de la lista tambien
                    this->child_list->ult_nodo = new_node;
                }
            }
            current = current->prev;
        }

    }
    return *new_node;
}

bool
Node::has_child_nodes() {
    if(child_list != NULL)
        if(child_list->n_nodos > 0)
            return true;
        else
            return false;
    else
        return false;
}


    Node
Node::remove_child(Node *old_child)	// raises(DOMException)		//FALTA TESTEAR
{
    Node *current = new Node();
    current = this->child_list->ult_nodo;
    if (this->has_child_nodes()) {
        if (this->child_list->search_node(old_child)) {
            while (current != NULL) {
                if (current == old_child) {
                    Node *previo = new Node();
                    Node *siguiente = new Node();
                    previo = current->prev;
                    siguiente = current->sgte;
                    if (previo != NULL)
                        old_child->prev = previo;

                    if (siguiente != NULL)
                        old_child->sgte = siguiente;
                    child_list->n_nodos--;
                }
            }
        }
    }
    return *old_child;
}


    bool
Node::has_attributes()
{
    if(this->attribute != "")
        return true;
    else
        return false;
}
