#ifndef __LIST_H__
#define __LIST_H__

#include <stdio.h>
#include <string>
#include "node.h"

struct Node;

using namespace std;

class List
{
public:
	int n_nodos;
	Node *ult_nodo;
        Node *first_node;

	List();
        void print_list();
	void insert_node_at_begin(Node *inicio);
        void insert_node_at_end(Node *new_node);
        bool search_node(Node *node);
        void remove_node(Node *old_node);
        bool search_node_by_type_and_name(string type,string label);
        Node get_node_by_type_and_name(string type, string tag);

};

#endif
