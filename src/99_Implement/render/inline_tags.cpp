#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <cstring>
#include <ncurses.h>

#include "definitions.h"
#include "styles.h"

/*!Output buffer*/
extern string gen_cont;

void render_title (Node *node) {
}

void render_a (Node *node) {
      gen_cont += node->content;
      if(node->child_list != NULL)
          render_list(node->child_list);
}

void render_abbr (Node *node) {
    gen_cont += node->content;
    if(node->child_list != NULL)
         render_list(node->child_list);

}

void render_b (Node *node) {
    gen_cont += node->content;
    if(node->child_list != NULL)
        render_list(node->child_list);

}

void render_big (Node *node) {
    gen_cont += node->content;
}

void render_button(Node *node){
    gen_cont += node->content;
}

void render_cite(Node *node){
     gen_cont += node->content;
}

void render_code(Node *node){
    gen_cont += node->content;
}

void render_dfn(Node *node){
    gen_cont += node->content;
}

void render_em(Node *node){
      gen_cont += node->content;
}

void render_i (Node *node) {
    gen_cont += node->content;
    if(node->child_list != NULL)
         render_list(node->child_list);
}

void render_img(Node *node){
    gen_cont += "[IMG]";
}

void render_input(Node *node){
    gen_cont += "[_       ]";
}

void render_kdb(Node *node){
      gen_cont += node->content;
}

void render_label(Node *node){
    gen_cont += "(  )";
}

void render_q(Node *node){
    gen_cont += node->content;
}

void render_select(Node *node){
    gen_cont += node->content;
}

void render_small(Node *node){
    gen_cont += node->content;
}

void render_span(Node *node){
    if(node->child_list != NULL)
        render_list(node->child_list);
    gen_cont += node->content;
}

void render_strong (Node *node) {
    gen_cont += node->content;
}

void render_textarea (Node *node){
}

void render_var (Node *node){
    gen_cont += node->content;
}


