#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <string.h>
#include <ncurses.h>
//Extern files
#include "definitions.h"

#define INLINE 30
#define INBLOCK 38

struct Node;

int total_lines = 0;

string gen_cont = "";

//!All types of inline tags
string inline_tags[100] = {
    "b","big","i","small","abbr","acronym","cite","code","em","kbd","strong","samp","time","var",
    "a","bdo","br","img","map","q","script","span","sub","sup","button","input","label","select","textarea","title"
};

//!All types of inblock tags
string inblock_tags[100] = {
    "address","blockquote","center","dir","div","dl","fieldset","form","h1","h2","h3","h4","h5","h6","hr",
    "insindex","menu","noframes","noscript","ol","p","pre","table","ul",
    "dd","dt","frameset","li","tbody","td","tr","tfoot","th","thread","tr","head","html","body"
};


//!Para hacer switch con char*
constexpr unsigned int str2int(const char* str, int h = 0)
{
    return !str[h] ? 5381 : (str2int(str, h+1) * 33) ^ str[h];
}

/*!Render a node root with child lists*/
string render_root(Node *root){
    element_node(root);
    return gen_cont;
}


/*! This function render a list of nodes
 *\param list: lista
 *\return void
 * */
void render_list(List *ls){
    if(ls != NULL){
        Node *temp = new Node();
        temp = ls->first_node;
        while(temp != NULL){
            element_node(temp);

            temp = temp->sgte;
        }
    }
}

//!Funcion que distingue entre una etiqueta inline o inblock
/*\param node: nodo etiqueta
 *\return void
 */
void element_node(Node *node){

    if(check_type(node->tag_name, 0)){
        //Inline
        inline_lb(node);
    }else if(check_type(node->tag_name, 1)){
        //Inblock
        inblock_lb(node);
    }else{
        //Default
        return;
    }
}



//!Funcion que compara todas las etiquetas
/*
   \param tag: Nombre de la etiqueta a comparar
   \param type: 0 tipo inline 1 tipo inblock
   \return bool
   */
bool check_type(string tag, int type){
    bool result = false;
    if(type == 0){
        for(int cnt = 0; cnt < INLINE; cnt++){
            if(inline_tags[cnt].compare(tag) == 0)
                result = true;
        }
    }else if(type == 1){
        for(int cnt = 0; cnt < INBLOCK; cnt++){
            if(inblock_tags[cnt].compare(tag) == 0)
                result = true;
        }
    }
    return result;
}

//!Funcion que trata las etiqueta inline
/*\param node: nodo etiqueta
 *\return void
 */
void inline_lb(Node *node){
    //Tratamos las etiquetas inline
    switch(str2int(&node->tag_name[0])){
        case str2int("title"):
            render_title(node);
            break;
        case str2int("a"):
            render_a(node);
            break;
        case str2int("abbr"):
            render_abbr(node);
            break;
        case str2int("big"):
            render_big(node);
            break;
        case str2int("button"):
            render_button(node);
            break;
        case str2int("cite"):
            render_cite(node);
            break;
        case str2int("code"):
            render_code(node);
            break;
        case str2int("dfn"):
            render_dfn(node);
            break;
        case str2int("em"):
            render_em(node);
            break;
        case str2int("i"):
            render_i(node);
            break;
        case str2int("img"):
            render_img(node);
            break;
        case str2int("input"):
            render_input(node);
            break;
        case str2int("kdb"):
            render_kdb(node);
            break;
        case str2int("label"):
            render_label(node);
            break;
        case str2int("q"):
            render_q(node);
            break;
        case str2int("select"):
            render_select(node);
            break;
        case str2int("small"):
            render_small(node);
            break;
        case str2int("span"):
            render_span(node);
            break;
        case str2int("strong"):
            render_strong(node);
            break;
        case str2int("textarea"):
            render_textarea(node);
            break;
        case str2int("var"):
            render_var(node);
            break;
    }
}

//!Funcion que trata las etiquetas inblock
/*\param node: nodo etiqueta
 *\return void
 */
void inblock_lb(Node *node){
    //Tratamos las etiquetas inblock
    switch(str2int(&node->tag_name[0])){
        case str2int("html"):
            render_html(node);
            break;
        case str2int("head"):
            render_head(node);
            break;
        case str2int("body"):
            render_body(node);
            break;
        case str2int("div"):
            total_lines++;
            render_div(node);
            break;
        case str2int("center"):
            break;
        case str2int("dd"):
            break;
        case str2int("dt"):
            break;
        case str2int("dl"):
            break;
        case str2int("h1"):
            render_h1(node);
            break;
        case str2int("h2"):
            render_h2(node);
            break;
        case str2int("h3"):
            render_h3(node);
            break;
        case str2int("h4"):
            render_h4(node);
            break;
        case str2int("h5"):
            render_h5(node);
            break;
        case str2int("h6"):
            render_h6(node);
            break;
        case str2int("li"):
            total_lines++;
            render_li(node);
            break;
        case str2int("ul"):
            total_lines++;
            render_ul(node);
            break;
        case str2int("tr"):
            total_lines++;
            render_tr(node);
            break;
        case str2int("td"):
            render_td(node);
            break;
        case str2int("p"):
            total_lines++;
            render_p(node);
            break;

    }
}

