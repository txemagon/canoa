#ifndef __LABEL_CONTROLLER_H__
#define __LABEL_CONTROLLER_H__
#include <string>
#include <strings.h>

using namespace std;

struct Node;
struct List;

void set_enable_colors();
void render_list(List *ls);
string render_root(Node *root);
bool check_type(string tag, int type);
void element_node(Node *node);
void inline_lb(Node *node);
void inblock_lb(Node *node);

#endif
