#ifndef __INBLOCK_TAGS_H__
#define __INBLOCK_TAGS_H__

class Node;

void render_html(Node *node);
void render_head(Node *node);
void render_body(Node *node);
void render_dl(Node *node);
void render_dd(Node *node);
void render_dt(Node *node);
void render_div(Node *node);
void render_p(Node *node);

void render_h1(Node *node);
void render_h2(Node *node);
void render_h3(Node *node);
void render_h4(Node *node);
void render_h5(Node *node);
void render_h6(Node *node);

void render_li(Node *node);
void render_ul(Node *node);
void render_tr(Node *node);
void render_td(Node *node);



#endif
