#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <ncurses.h>

#include "definitions.h"
#include "styles.h"

/*!output buffer*/
extern string gen_cont;


void render_html(Node *node){
    render_list(node->child_list);
}

void render_head(Node *node){
    render_list(node->child_list);
}

void render_body(Node *node){
    render_list(node->child_list);
}

void render_div(Node *node){
    gen_cont += "\n";
    render_list(node->child_list);
}

void render_dl(Node *node){
    gen_cont += node->content;
    if(node->child_list != NULL)
        render_list(node->child_list);

}
void render_dd(Node *node){
    gen_cont += node->content;
}
void render_dt(Node *node){
gen_cont += node->content;
}

void render_footer(Node *node){
    if(node->child_list != NULL)
        render_list(node->child_list);

}

void render_form(Node *node){
    if(node->child_list != NULL)
        render_list(node->child_list);

}

void render_header(Node *node){
    if(node->child_list != NULL)
        render_list(node->child_list);

}

void render_hr(Node *node){

}

void render_p(Node *node){
    gen_cont += node->content;
    if(node->child_list != NULL)
        render_list(node->child_list);

}

void render_h1(Node *node){
    gen_cont += node->content;
    render_list(node->child_list);
}
void render_h2(Node *node){
    gen_cont += node->content;
    render_list(node->child_list);
}
void render_h3(Node *node){
    string all_content = "";
    all_content += node->content;
    if(node->child_list != NULL){
        Node *temp = new Node();
        temp = node->child_list->first_node;
        all_content += temp->content;

        temp = temp->sgte;
    }
    gen_cont += all_content;
}
void render_h4(Node *node){
    gen_cont += node->content;
    render_list(node->child_list);
}
void render_h5(Node *node){
    gen_cont += node->content;
    render_list(node->child_list);
}
void render_h6(Node *node){
    gen_cont += node->content;
    render_list(node->child_list);
}

void render_ol(Node *node){

}

void render_li(Node *node){
    gen_cont += "\t";
    gen_cont += "* ";
    if(node->child_list != NULL)
        render_list(node->child_list);
    gen_cont += "\n";
}

void render_ul(Node *node){
    gen_cont += "\n";
    if(node->child_list != NULL)
        render_list(node->child_list);
}

void render_tr(Node *node){
    if(node->child_list != NULL)
         render_list(node->child_list);
    gen_cont += "\n";
}

void render_td(Node *node){
    gen_cont += node->content;
    if(node->child_list != NULL)
         render_list(node->child_list);
}
