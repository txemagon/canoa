#include <stdio.h>
#include <curl/curl.h>

#include "definitions.h"

#define ARCHIVO "pagina.txt"
#define MAX 200

void ask_url(){
    char url[200];
    scanf("%s", url);
    get_html(url);
}

/*! Función encargada de hacer la petición al servidor
 * y meter los datos en un archivo
 * */
void get_html(char url[200]){
    CURL *curl;
    CURLcode res;
    curl = curl_easy_init();
    FILE *fp =  fopen("pagina.html", "w");

    if(fp == NULL){
        fprintf(stderr, "Could not create file");
    }

    if(curl) {
        curl_easy_setopt(curl, CURLOPT_URL, url);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);

        res = curl_easy_perform(curl);

        check_errors_get_html(res);

        curl_easy_cleanup(curl);
    }
    fclose(fp);

}


