#ifndef __GET_HTML_H__
#define __GET_HTML_H__

#include <stdio.h>
#include <curl/curl.h>

void ask_url();
void get_html(char *url);
void check_errors_get_html(CURLcode res);

#endif

