#include <ncurses.h>
#include <stdlib.h>
#include <curl/curl.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <menu.h>

#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#include "../definitions.h"

