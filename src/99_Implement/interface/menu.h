#ifndef __MENU_H__
#define __MENU_H__

#include <ncurses.h>
#include <string>

class Node;
class List;

void url_window();
WINDOW *static_url_window();
void menu_window(Node *root, List *total_list);
void check_errors_get_html(CURLcode res);

#endif
