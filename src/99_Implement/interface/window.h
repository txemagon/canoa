#ifndef __WINDOW_H__
#define __WINDOW_H__

class Node;
class List;

void listener(int option, List *total_list, Node *root);
void web_window(Node *root);
void nodes_window(List *total_list);
void total_window(List *total_list);
void dom_window(Node *root);
void errors_window(List *total_list);


#endif
