#include "definitions.h"


#define A 100
/*!funcion que crear una caja segun las medidas que le indiques*/
WINDOW *create_newwin(int height, int width, int start_y, int start_x){
    WINDOW *win;

    win = newwin(height, width, start_y, start_x);
    box(win, 0, 0);

    wrefresh(win);

    return win;
}


