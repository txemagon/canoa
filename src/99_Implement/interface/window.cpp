#include "definitions.h"
#include <sstream>

string content = "";
string tags_content = "";

int move_x=0, move_y=0;
int time_scroll = 0;
bool already_done = true;


void listener(int option, List *total_list, Node *root){
    int c;
    wmove(stdscr, 0,0);
    while((c = getch()) != KEY_F(2)){
        switch(c){
            case KEY_LEFT:
                time_scroll++;
                refresh();
                break;
            case KEY_RIGHT:
                time_scroll--;
                refresh();
                break;
        }
        switch(option){
            case 1:
                clear();
                web_window(root);
                refresh();
                break;
            case 2:
                clear();
                nodes_window(total_list);
                refresh();
                break;
            case 3:
                clear();
                dom_window(root);
                refresh();
                break;
            case 4:
                clear();
                total_window(total_list);
                refresh();
                break;
            case 5:
                clear();
                errors_window(total_list);
                refresh();
                break;

        }
    }

}
string pre_string;

string cut_string(string content, int times_scroll){
    int contador=0;
    string new_content;
    istringstream iss(content);
    string line;

    if(time_scroll < 0){
        time_scroll = 0;
    }

    while(getline(iss, line)){
        contador++;
        if(contador >= times_scroll > 0 && contador <= 70 + times_scroll){
            new_content += line + "\n";
        }
    }
    pre_string = new_content;

    return new_content;
}

void web_window(Node *root){
    mvprintw(2,1, "WEB PAGE");
    mvprintw(
            2,
            145,
            "Pulsar F2 para ver otras opciones"
            );

    //box(stdscr, 0 , 50);
    content = "";
    string scroll_content = "";
    content = render_root(root);
    scroll_content = cut_string(content, time_scroll);
    mvprintw(5, 2, &scroll_content[0]);

    /*barra de la url*/
    WINDOW *static_url;
    static_url = static_url_window();

    box(static_url, 0, 0);
    move(5,0);
    whline(stdscr, ACS_HLINE, 250);
    wrefresh(stdscr);
    wrefresh(static_url);

    export_web(content);
}

void nodes_window(List *total_list){
    mvprintw(2,1, "NODES LIST");
    mvprintw(
            2,
            145,
            "Pulsar F2 para ver otras opciones"
            );
    //box(stdscr, 0 , 0);
    content = "";
    string scroll_content = "";
    content = print_list_tags(total_list);             /*outprint.cpp*/
    scroll_content = cut_string(content, time_scroll);

    mvwprintw(stdscr, 5, 2, &scroll_content[0]);

    /*barra de la url*/
    WINDOW *static_url;
    static_url = static_url_window();

    box(static_url, 0, 0);
    move(5,0);
    whline(stdscr, ACS_HLINE, 250);
    wrefresh(stdscr);
    wrefresh(static_url);


}

void dom_window(Node *root){
    mvprintw(2, 1, "DOM STRUCT");
    mvprintw(
            2,
            145,
            "Pulsar F2 para ver otras opciones"
            );
    content = "";
    string scroll_content = "";
    content = recursive_read(root);             /*outprint.cpp*/
    scroll_content = cut_string(content, time_scroll);

    mvwprintw(stdscr, 5, 5, &scroll_content[0]);

    /*barra de la url*/
    WINDOW *static_url;
    static_url = static_url_window();

    box(static_url, 0, 0);
    move(5,0);
    whline(stdscr, ACS_HLINE, 250);
    wrefresh(stdscr);
    wrefresh(static_url);

    export_dom_struct(content);
}

void total_window(List *total_list){
    mvprintw(2, 1, "TOTAL TAGS");
    mvprintw(
            2,
            145,
            "Pulsar F2 para ver otras opciones"
            );

    // box(stdscr, 0 , 0);

    if(already_done == true){       //To dont duplicate
        tags_content = counter_nodes(total_list);        /*parser/checker.cpp*/
        already_done = false;
    }


    string scroll_content = "";

    scroll_content = cut_string(tags_content, time_scroll);
    mvprintw(5, 2, &scroll_content[0]);

    /*barra de la url*/
    WINDOW *static_url;
    static_url = static_url_window();

    box(static_url, 0, 0);
    move(5,0);
    whline(stdscr, ACS_HLINE, 250);
    wrefresh(stdscr);
    wrefresh(static_url);

}

void errors_window(List *total_list){
    mvprintw(2,1, "TAGS ERRORS");
    mvprintw(
            2,
            145,
            "Pulsar F2 para ver otras opciones"
            );

    content = "";

    content = scroll_list(total_list);          /*parser/checker.cpp*/ /*Muestra los errores de cierre si hay*/
    if(content != "")
        mvprintw(10, 5, &content[0]);
    else
        mvprintw(10, 2, "HTML bien elaborado");


    /*barra de la url*/
    WINDOW *static_url;
    static_url = static_url_window();

    box(static_url, 0, 0);
    move(5,0);
    whline(stdscr, ACS_HLINE, 250);
    wrefresh(stdscr);
    wrefresh(static_url);

}
