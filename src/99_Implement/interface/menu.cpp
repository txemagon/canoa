#include "definitions.h" /*se incluye el archivo donde apunta a las funciones*/

#define ARCHIVO "pagina.html" /*Archivo donde pone el HTML que hemos solicitado*/
#define X 200 /*Maximo de caracteres que se puede poner en la URL */

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))

using namespace std;

class Node;
class List;

//Variables globales
int height = 0, width= 0, start_y = 0, start_x= 0;
int altura = 3, ancho= 100, y = 1, x = 18;
char URL[X];


/*crear un switch con char * */
constexpr unsigned int str2int(const char* str, int h = 0)
{
    return !str[h] ? 5381 : (str2int(str, h+1) * 33) ^ str[h];
}

/*En esta parte indicamos las opciones que estaran en el recuadro de la funcion menu*/
const char *opciones[] = {
    "-ver web",
    "-ver lista de nodos",
    "-ver estructura dom",
    "-ver etiquetas totales",
    "-ver posibles errores del html",
    "-nueva busqueda",
    "Salir",
    (char *)NULL,
};


WINDOW *static_url_window(){
    string final_string;
    string conv_url(URL);
    final_string = "URL : " + conv_url;
    WINDOW *static_url;

    static_url = create_newwin(altura, ancho, y, x);
    mvwprintw(static_url, 1, 1 , &final_string[0]);
    return static_url;
}



void url_window(){
    WINDOW *win2;

    char msg[] = "Ingrese una URL: ";
    CURL *curl;
    CURLcode res;
    FILE *fp = fopen(ARCHIVO, "w");

    mvprintw(2,2,"%s", msg); /*Se imprime el mensaje, en la posicion indicada*/

    win2 = create_newwin(altura, ancho, y, x); /*Se crea un recuadro donde el usuario ingresa la URL*/

    curl = curl_easy_init();

    mvwprintw(win2, 1, 19, URL);
    getstr(URL); /* con getstr obtenemos la cadena que el usuario ingrese en este caso la URL*/
    if (curl) {
        curl_easy_setopt(curl, CURLOPT_URL, URL);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);

        res = curl_easy_perform(curl);

        check_errors_get_html(res);
        curl_easy_cleanup(curl);

    }
    fclose(fp);
}

/*Comprueba si la petición a la pagina solicitada a devuelto el html o no*/
void check_errors_get_html(CURLcode res){
    WINDOW *my_win;
    char msg_error[X] = "Error no se ha podido enviar la petición a la pagina solicitada";
    char msg_V[X] = "Cargando pagina solicitada...";

    if(res != CURLE_OK){
        my_win = create_newwin(3, 100, 20, 40);
        mvwprintw(my_win, 1,20, msg_error);
    }
    else{
        my_win = create_newwin(3,100,20,40);
        mvwprintw(my_win, 1,20, msg_V);
    }
    wrefresh(my_win);

}


void menu_window(Node *root, List *total_list){
    /*variables menu*/
    ITEM **my_items;
    int c;
    MENU *my_menu;
    ITEM *cur_item;
    WINDOW *new_win;
    int n_choices, i;

    initscr();
    cbreak();
    noecho();
    keypad(stdscr, TRUE);

    n_choices = ARRAY_SIZE(opciones);
    my_items = (ITEM **)calloc(n_choices, sizeof(ITEM *));

    for(i = 0; i < n_choices; ++i){
        my_items[i] = new_item(opciones[i],""); /*Se indica a vacio, porque si no crearia 2 veces las opciones.*/
    }

    my_menu = new_menu((ITEM **)my_items);

    new_win = newwin(13,40,15,70); // recuadro creado para el menu
    keypad(new_win, TRUE);

    set_menu_win(my_menu, new_win);
    set_menu_sub(my_menu, derwin(new_win, 10,38,3,1)); // asignamos el menu al cuadrado indicandole con valores
    set_menu_format(my_menu,7,1);
    set_menu_mark(my_menu, "*");

    box(new_win, 0,0);
    menu_opts_off(my_menu, O_SHOWDESC);
    refresh();


    post_menu(my_menu);
    wrefresh(new_win);



    while((c = getch()) != KEY_F(1))
    {   switch(c)
        {
            case KEY_DOWN:
                menu_driver(my_menu, REQ_DOWN_ITEM);
                break;
            case KEY_UP:
                menu_driver(my_menu, REQ_UP_ITEM);
                break;
            case 10:
                switch(str2int(item_name(current_item(my_menu))))
                {
                    case str2int("-ver web"):
                        clear();
                        listener(1, total_list, root);
                        break;
                    case str2int("-ver lista de nodos"):
                        clear();
                        listener(2, total_list, root);
                        refresh();
                        break;
                    case str2int("-ver estructura dom"):
                        clear();
                        listener(3, total_list, root);
                        break;
                    case str2int("-ver etiquetas totales"):
                        clear();
                        listener(4, total_list, root);
                        break;
                    case str2int("-ver posibles errores del html"):
                        clear();
                        listener(5, total_list, root);
                        break;
                    case str2int("-nueva busqueda"):
                        clear();
                        system("./ej_canoa");
                        break;
                    case str2int("Salir"):
                        system("exit");
                        break;
                }
                post_menu(my_menu);
                box(new_win, 0, 0);
                wrefresh(new_win);
                break;
        }
        wrefresh(new_win);
    }

    unpost_menu(my_menu);
    free_menu(my_menu);
    for(i=0; i<n_choices; i++)
        free_item(my_items[i]);

    endwin();
}

/*Compilación --> gcc main.cpp interface.cpp -o interface -lncurses -lcurl || make*/
