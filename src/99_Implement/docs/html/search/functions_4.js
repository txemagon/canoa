var searchData=
[
  ['find_5fall_5fcomments_300',['find_all_comments',['../omite__lines_8h.html#affb410e5b03fb67d622db30a095638e8',1,'find_all_comments(string content, int pos):&#160;omite_lines.h'],['../parser_8h.html#affb410e5b03fb67d622db30a095638e8',1,'find_all_comments(string content, int pos):&#160;parser.h']]],
  ['find_5fall_5fstyles_301',['find_all_styles',['../parser_8h.html#a87615fb7d1debec45d4b1c3145e40e63',1,'parser.h']]],
  ['find_5fdoctype_302',['find_doctype',['../omite__lines_8h.html#ac1e5c3fbe3851b03a76fb50b107dd5ee',1,'find_doctype(string content, int pos):&#160;omite_lines.h'],['../parser_8h.html#ac1e5c3fbe3851b03a76fb50b107dd5ee',1,'find_doctype(string content, int pos):&#160;parser.h']]],
  ['find_5fgap_5ftags_303',['find_gap_tags',['../omite__lines_8cpp.html#a192e9afa31735cb40fdb150f2d26d1a2',1,'find_gap_tags(string content, int pos, string open_tag, string close_tag):&#160;omite_lines.cpp'],['../omite__lines_8h.html#a192e9afa31735cb40fdb150f2d26d1a2',1,'find_gap_tags(string content, int pos, string open_tag, string close_tag):&#160;omite_lines.cpp']]]
];
