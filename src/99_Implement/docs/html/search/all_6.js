var searchData=
[
  ['gen_5fcont_60',['gen_cont',['../inblock__tags_8cpp.html#aede9ac10db6f63e0376e2c4aa4fef8a4',1,'gen_cont():&#160;tags_controller.cpp'],['../inline__tags_8cpp.html#aede9ac10db6f63e0376e2c4aa4fef8a4',1,'gen_cont():&#160;tags_controller.cpp'],['../tags__controller_8cpp.html#aede9ac10db6f63e0376e2c4aa4fef8a4',1,'gen_cont():&#160;tags_controller.cpp']]],
  ['general_5fbuffer_61',['general_buffer',['../outprint_8cpp.html#a936e4f84f9929bbc437f9e5c724e8d29',1,'outprint.cpp']]],
  ['general_5fcontent_62',['general_content',['../parser_8cpp.html#a554b42db2bff8cbb10bf9e7cf84f8117',1,'parser.cpp']]],
  ['get_5fhtml_63',['get_html',['../get__html_8cpp.html#abf914de4661cd7faeb62aa479f1ccddf',1,'get_html(char url[200]):&#160;get_html.cpp'],['../get__html_8h.html#a2e023e32ebb643b5d3c4b1068067e204',1,'get_html(char *url):&#160;get_html.h']]],
  ['get_5fhtml_2ecpp_64',['get_html.cpp',['../get__html_8cpp.html',1,'']]],
  ['get_5fhtml_2eh_65',['get_html.h',['../get__html_8h.html',1,'']]],
  ['get_5fnode_5fby_5ftype_5fand_5fname_66',['get_node_by_type_and_name',['../classList.html#adde4367fb3b9bee34fa19d908b1c44e9',1,'List']]],
  ['get_5ftag_5fnumber_67',['get_tag_number',['../checker_8cpp.html#ab9ad38f4426aac8bff1f058295d45ffd',1,'get_tag_number(string nombre, string tipo):&#160;checker.cpp'],['../checker_8h.html#ab9ad38f4426aac8bff1f058295d45ffd',1,'get_tag_number(string nombre, string tipo):&#160;checker.h']]],
  ['green_68',['GREEN',['../styles_8h.html#acfbc006ea433ad708fdee3e82996e721',1,'styles.h']]]
];
