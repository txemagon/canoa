var searchData=
[
  ['black_11',['BLACK',['../styles_8h.html#a7b3b25cba33b07c303f3060fe41887f6',1,'styles.h']]],
  ['blue_12',['BLUE',['../styles_8h.html#a79d10e672abb49ad63eeaa8aaef57c38',1,'styles.h']]],
  ['boldblack_13',['BOLDBLACK',['../styles_8h.html#aef2fe95894117165b29036718221979f',1,'styles.h']]],
  ['boldblue_14',['BOLDBLUE',['../styles_8h.html#a11e77c19555cbd15bcc744ff36a18635',1,'styles.h']]],
  ['boldcyan_15',['BOLDCYAN',['../styles_8h.html#ae87af5e6363eb1913b17f24dcb60a22d',1,'styles.h']]],
  ['boldgreen_16',['BOLDGREEN',['../styles_8h.html#a4a6c893a1703c33ede7d702fe5f97c91',1,'styles.h']]],
  ['boldmagenta_17',['BOLDMAGENTA',['../styles_8h.html#ac4723c5ee12cfca16e2172b57b99cb07',1,'styles.h']]],
  ['boldred_18',['BOLDRED',['../styles_8h.html#ab912d02c7998c3d47d05f87be4e2c920',1,'styles.h']]],
  ['boldwhite_19',['BOLDWHITE',['../styles_8h.html#aa4ef051614aa0bd503b0a18ee158c5d7',1,'styles.h']]],
  ['boldyellow_20',['BOLDYELLOW',['../styles_8h.html#a8cec79108dfc3c61e8e32d390ec28b26',1,'styles.h']]]
];
