var searchData=
[
  ['carga_21',['Carga',['../interface_8h.html#a92758ef29790c3a1a97e533f38706bd5',1,'interface.h']]],
  ['check_5fchild_5ftag_22',['check_child_tag',['../legacy_8cpp.html#a0e4b863a5747e88cc600bf54a149fe82',1,'check_child_tag(Node *root, Node *temp):&#160;legacy.cpp'],['../legacy_8h.html#a0e4b863a5747e88cc600bf54a149fe82',1,'check_child_tag(Node *root, Node *temp):&#160;legacy.cpp']]],
  ['check_5ferrors_5fget_5fhtml_23',['check_errors_get_html',['../get__html_8h.html#a18764ac8db2487ec0d911769f86a0ebc',1,'check_errors_get_html(CURLcode res):&#160;menu.cpp'],['../interface_8h.html#a18764ac8db2487ec0d911769f86a0ebc',1,'check_errors_get_html(CURLcode res):&#160;menu.cpp'],['../menu_8cpp.html#a18764ac8db2487ec0d911769f86a0ebc',1,'check_errors_get_html(CURLcode res):&#160;menu.cpp'],['../menu_8h.html#a18764ac8db2487ec0d911769f86a0ebc',1,'check_errors_get_html(CURLcode res):&#160;menu.cpp']]],
  ['check_5fignore_5ftags_24',['check_ignore_tags',['../purgate_8cpp.html#a7890aef40ea7498d5a2faae09d6bb29b',1,'check_ignore_tags(std::string tag_name):&#160;purgate.cpp'],['../purgate_8h.html#a7890aef40ea7498d5a2faae09d6bb29b',1,'check_ignore_tags(std::string tag_name):&#160;purgate.cpp']]],
  ['check_5ftype_25',['check_type',['../tags__controller_8cpp.html#a417bbcf0ab41ff73f6a95a0e592902e2',1,'check_type(string tag, int type):&#160;tags_controller.cpp'],['../tags__controller_8h.html#a417bbcf0ab41ff73f6a95a0e592902e2',1,'check_type(string tag, int type):&#160;tags_controller.cpp']]],
  ['checker_2ecpp_26',['checker.cpp',['../checker_8cpp.html',1,'']]],
  ['checker_2eh_27',['checker.h',['../checker_8h.html',1,'']]],
  ['child_5flist_28',['child_list',['../classNode.html#a05cc7ae6975892d9e18db0c6f00d6391',1,'Node']]],
  ['clean_5fstring_29',['clean_string',['../legacy_8cpp.html#a084b44b37dc722b1c6074c68685924b8',1,'clean_string(string content):&#160;legacy.cpp'],['../legacy_8h.html#a42129019cdedaaf34ba0b54aaef47b82',1,'clean_string(std::string content):&#160;legacy.cpp']]],
  ['clone_5fnode_30',['clone_node',['../classNode.html#a2bd21683fa0348e5d64623d45107a64a',1,'Node::clone_node(bool deep)'],['../classNode.html#a43253043190509ed9406176d17d4295a',1,'Node::clone_node(Node *new_node)']]],
  ['close_31',['close',['../legacy_8cpp.html#a3caa688207c8d100c59275ee6aaa2d11',1,'close():&#160;legacy.cpp'],['../purgate_8cpp.html#a3caa688207c8d100c59275ee6aaa2d11',1,'close():&#160;legacy.cpp']]],
  ['cnt2_32',['cnt2',['../outprint_8cpp.html#a76492dda1e4a26f2c3ecc6cc59338faa',1,'outprint.cpp']]],
  ['cnt_5fid_33',['cnt_id',['../parser_8cpp.html#ab8dbbcd3a867432a9b7d016a920233f7',1,'parser.cpp']]],
  ['colect_5fcaracter_34',['colect_caracter',['../parser_8cpp.html#acdc3e694c659c03d60e614e814b910cc',1,'colect_caracter(char caracter):&#160;parser.cpp'],['../parser_8h.html#acdc3e694c659c03d60e614e814b910cc',1,'colect_caracter(char caracter):&#160;parser.cpp']]],
  ['cont_5fvect_35',['cont_vect',['../omite__lines_8cpp.html#a73479acb83c1766ce9ff99ee57455ecb',1,'omite_lines.cpp']]],
  ['content_36',['content',['../classNode.html#ad8dd328f09f4f146442a490b134db022',1,'Node::content()'],['../window_8cpp.html#aaa48247c6e717970fa828bc18b7a1a16',1,'content():&#160;window.cpp']]],
  ['counter_5fnodes_37',['counter_nodes',['../checker_8cpp.html#a7e4286ee75d5b0c72b1699df94725f4c',1,'counter_nodes(List *ls):&#160;checker.cpp'],['../checker_8h.html#a7e4286ee75d5b0c72b1699df94725f4c',1,'counter_nodes(List *ls):&#160;checker.cpp']]],
  ['create_5fchild_5fstruct_38',['create_child_struct',['../legacy_8cpp.html#ac6e3f7235453c67e9ab8db7dc92f8411',1,'create_child_struct(List *ls):&#160;legacy.cpp'],['../legacy_8h.html#ac6e3f7235453c67e9ab8db7dc92f8411',1,'create_child_struct(List *ls):&#160;legacy.cpp']]],
  ['create_5fnewwin_39',['create_newwin',['../interface_8cpp.html#ad4fb646518b73d4c2c2ab9d1df451be6',1,'create_newwin(int height, int width, int start_y, int start_x):&#160;interface.cpp'],['../interface_8h.html#ad4fb646518b73d4c2c2ab9d1df451be6',1,'create_newwin(int height, int width, int start_y, int start_x):&#160;interface.cpp']]],
  ['cut_5fstring_40',['cut_string',['../window_8cpp.html#a90ca4c192dba7900380ddd52ffdec0c7',1,'window.cpp']]],
  ['cyan_41',['CYAN',['../styles_8h.html#ad243f93c16bc4c1d3e0a13b84421d760',1,'styles.h']]]
];
