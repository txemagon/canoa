var indexSectionsWithContent =
{
  0: "abcdefghilmnoprstuvwxy",
  1: "ln",
  2: "cdegilmnopstw",
  3: "acdefghilmnoprstuw",
  4: "acdfghilmnopstuvwxy",
  5: "abcgimnruwxy"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Macros"
};

