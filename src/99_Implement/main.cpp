#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <unistd.h>

#include "definitions.h"

int main(int argc, char* argv[]){

    char* docname;

    //!variable para la entrada de F1 para sacar el menu
    int ch;

    /*!variables de ventana*/
    WINDOW *my_win;
    int row, col;

    initscr(); /*!iniciar modo ncurses*/
    raw(); /*!Permite escribir*/
    refresh();
    my_win = create_newwin(0, 0, 0, 0); /*!Se crea un recuadro alrededor de la terminal*/
 //   mvprintw(
 //             2,
 //             145,
 //             "Pulsar F2 para ver otras opciones"
 //           );


    /*!Inicializamos la lista y el array*/
    List *total_list = new List();
    Node *root = new Node();



    /*!Ventana de la url*/
    url_window();

    start_parse_file(docname, total_list);  /*!parser/parser.cpp*/

    *root = create_child_struct(total_list);  /*!parser/legacy.cpp*/

    sleep(2);
    clear();
    menu_window(root, total_list);
    wrefresh(my_win);
    cbreak();

    /*!Menu de opciones*/
    ch = getch();
    if (ch = KEY_F(2))
        menu_window(root, total_list); //!pulsado F1 abrimos el menu con diferentes opciones

    refresh();
    endwin();





    return EXIT_SUCCESS;
}
