#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "definitions.h"

int num = 0;
int cnt2 = 0;
string anotation = "";
string general_buffer;

/*!This function print into a string all the data of the list tags*/
string print_list_tags(List *ls){
    string content = "";
    Node *temp = new Node();
    temp = ls->first_node;
    while(temp != NULL){

        content += temp->tag_name + " - " + temp->tag_type + " - " + to_string(temp->id) + "\n";

        temp = temp->sgte;
    }
    return content;
}

/*!This only print the anotation of the visual arrow*/
void print_anotation(){
    anotation = "";
    for(int cnt=0; cnt<=num; cnt++){
        anotation += "    ";
    }
    anotation += "+-->";

}

/*This is the main function to render the dom struct, it combine the
 * names of the tag with the anotation and then insert into string*/
string read_childs(Node *temp, string content){
    num += 1;
    while(temp != NULL){
        print_anotation();
        content += anotation + temp->tag_name + "\n";

        if(temp->child_list != NULL){
            general_buffer += read_childs(temp->child_list->first_node, content);
            content = "";
        }
        temp = temp->sgte;
    }
    num += -1;
    return content;
}

/*We use this function to create the recursion*/
string recursive_read(Node *root){
    string content = "";

    content += root->tag_name + "\n";

    if(root->child_list != NULL)
        content += read_childs(root->child_list->first_node, content);

    return general_buffer;
}



