#include <stdio.h>
#include <stdlib.h>
#include <string>

#include "definitions.h"

#define N_IGN_TAGS 3

using namespace std;

extern string close;
extern string open;


string ignore_tags[N_IGN_TAGS] = {"style", "source","track"};


/*! With this function we remove the innecesary nodes like the not close tags
 * */
void purgate_list(List *ls){
    Node *temp = new Node();
    temp = ls->first_node;
    while(temp != NULL){
        if(not_close_tag(temp->tag_name) && temp->tag_type == close && check_ignore_tags(temp->tag_name) == true)
            ls->remove_node(temp);

        temp = temp->sgte;
    }
}

/*This function check if the tag_name is one of the ignore tags*/
bool check_ignore_tags(std::string tag_name){
    for(int cnt = 0; cnt < N_IGN_TAGS; cnt++){
        if(ignore_tags[cnt] == tag_name)
            return true;
    }
    return false;
}

