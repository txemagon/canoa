#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <strings.h>
//Own docs
#include "definitions.h"

#define UNCLOSED_TAGS 8

string close = "close";
string open = "open";

/*!It works like a launcher to start building the dom struct*/
Node create_child_struct(List *ls){
    Node *root = new Node();
    root = ls->first_node;

    purgate_list(ls);

    check_child_tag(root, root->sgte);

    return *root;
}

/*This is the most important function, funciona de una forma recursiva de manera que si encuentra una
 * etiqueta de apertura antes que la propia de cierre vuelve a llamar a la función y vuelve a repetir el proceso
 * hasta que encuentre su propia etiqueta de cierre, en ese momento retorna un nodo que es introducido en la etiqueta
 * anterior(que sería su etiqueta padre) -- También tiene en cuenta las etiquetas sin cierre que tan solo las introduce
 * y no entra en forma recursiva.
 * */
Node check_child_tag(Node *root, Node *temp){
    while(temp != NULL){

        if(temp->tag_name == root->tag_name && temp->tag_type == close)
            return *temp;
        else {
            Node *new_node = new Node();
            if(not_close_tag(temp->tag_name))       //Solo para etiquetas sin cierre
                *new_node = *temp;
            else
                *new_node = check_child_tag(temp, temp->sgte);
            Node *other = new Node();               //Nodo al que clonamos para que no tenga misma dirección de memoria
            other->clone_node(temp);
            other->sgte = NULL;
            other->content = clean_string(new_node->content);
            root->insert_child_at_end(other);

            temp = new_node->sgte;
        }
    }
    return *temp;
}

/*!This function only contains a list of tags without close and it check if the
 * tag is unclose or not*/
bool not_close_tag(string tag){
    string unclose_tags[UNCLOSED_TAGS] = {"p","meta","link","input","img", "br", "hr", "path"};
    for(int cnt=0; cnt < UNCLOSED_TAGS; cnt++)
        if(unclose_tags[cnt] == tag)
            return true;
    return false;
}

/*!Simple function to remove inncesaries caracter like \n \t \r etc..
 * it retuns a new string clean
 * */
string clean_string(string content){
    string new_content;
    for(int cnt=0; cnt < content.size(); cnt++){
        if(content.at(cnt) != '\n' && content.at(cnt) != '\t')
            new_content += content[cnt];
    }
    return new_content;
}
