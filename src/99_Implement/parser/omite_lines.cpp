#include <stdlib.h>
#include <stdio.h>
#include <string>

#include "definitions.h"

using namespace std;

int vect[500][2];    //To get comment init and comment close position</pre>
int cont_vect = 0;

/*! Open the file and clone to a string to call the recursive function
 * find_all_comments
 * */
void omite_pieces(string content){
    find_gap_tags(content, 0, "<style", "</style>");
    find_gap_tags(content, 0, "<script", "</script>");
    find_gap_tags(content, 0, "<!--", "-->");
}

/*! Search all the tags lines and insert into a vector the position when the comment
 *  begin and when end vect[][0] --> Begin position, vect[][1] --> End position
 * */
void find_gap_tags(string content, int pos, string open_tag, string close_tag){
    int pos_open = content.find(open_tag, pos);
    int pos_close = content.find(close_tag, pos);

    if(pos_open != string::npos && pos_close != string::npos){
        vect[cont_vect][0] = pos_open;
        vect[cont_vect][1] = pos_close + close_tag.size() - 1;

        cont_vect++;
        find_gap_tags(content, pos_close + close_tag.size() - 1, open_tag, close_tag);
    }else{
        return;
    }
}


/*! Indicate if can parse that concret position of the caracter
 * */
bool parse_that_position(int position){
    for(int cnt=0; cnt < 500; cnt++){
        if(position == 0)
            return true;
        if(position >= vect[cnt][0] && position <= vect[cnt][1])
            return false;
    }
    return true;
}
