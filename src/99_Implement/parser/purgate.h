#ifndef __PURGATE_H__
#define __PURGATE_H__

class List;

void purgate_list(List *ls);
bool check_ignore_tags(std::string tag_name);


#endif
