#ifndef __CHECKER_H__
#define __CHECKER_H__

class List;

string scroll_list(List *ls);
string counter_nodes(List *ls);
bool exist_in_array(List *ls, Node *node);
int get_tag_number(string nombre, string tipo);
string print_result();

#endif
