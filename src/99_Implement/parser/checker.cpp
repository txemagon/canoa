#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <string>
#include "definitions.h"

class Node;
class List;



/*TO COUNT THE NODES IN TOTAL
 * ------------------------*/

std::string nodes[750][3];
int pos = 0;
int totales;


/*!This function check if the node exists in the list*/
bool exist_in_array(List *ls, Node *node){
    for(int cnt = 0; cnt < 750; cnt++){
        if(node->tag_name == nodes[cnt][0] && node->tag_type == nodes[cnt][1]){
            pos = cnt;
            return true;
        }
    }
    return false;
}


/*!This function return a string with all the data*/
string print_result(){
    string content;
    for(int cnt=0; cnt<750; cnt++){
        if(nodes[cnt][0] != "")
          content += "Nombre: " + nodes[cnt][0] + " - Tipo: " + nodes[cnt][1] + " - Numero: " + nodes[cnt][2] + "\n";
    }
    return content;
}


/*!This is the important function to count nodes,
 * it sumarize if the node exists or insert if doesnt exists*/
string counter_nodes(List *ls){

    Node *temp = new Node();
    temp = ls->first_node;
    while(temp != NULL){
        if(exist_in_array(ls, temp)){
            char* cad = strdup(nodes[pos][2].c_str());    //strdup to convert string to char*
            int act = atoi(cad);
            nodes[pos][2] = to_string(act + 1);
        }else{
            nodes[totales][0] = temp->tag_name;
            nodes[totales][1] = temp->tag_type;
            nodes[totales][2] = "1";
            totales++;
        }
        temp = temp->sgte;
    }
    return print_result();
}

/*!It returns the tag cuantity of the tag*/
int get_tag_number(string nombre, string tipo) {
    for(int cnt=0; cnt < 750; cnt++){
        if(nodes[cnt][0] == nombre && nodes[cnt][1] == tipo){
            int number;
            return stoi(nodes[cnt][2]);
        }
    }
    return 0;
}


/*!Simple loop that check if every tag has close one, if not
 * print a error list*/
string scroll_list(List *ls){
    std::string errors;
    Node *temp = new Node();
    temp = ls->first_node;
    while(temp != NULL){
        if(!ls->search_node_by_type_and_name("close", temp->tag_name) && !not_close_tag(temp->tag_name)
                && check_ignore_tags(temp->tag_name) == false)
            errors += temp->tag_name + " no tiene cierre\n";

        temp = temp->sgte;
    }
    return errors;
}

