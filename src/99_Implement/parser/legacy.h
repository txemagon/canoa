#include <string>
#ifndef __LEGACY_H__
#define __LEGACY_H__

class List;

Node create_child_struct(List *ls);
Node check_child_tag(Node *root, Node *temp);
bool not_close_tag(std::string label);
std::string clean_string(std::string content);

#endif
