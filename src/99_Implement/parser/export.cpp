#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <fstream>

using namespace std;

/*!Export to a file dom_struct.pdf the content of the option dom struct*/
void export_dom_struct(string content){
    ofstream myfile;
    myfile.open ("./files_exported/dom_struct.pdf");
    myfile << content;
    myfile.close();
}

/*!Export to a file list_nodes.pdf the content of the option list of nodes*/
void export_list_nodes(string content){
    ofstream myfile;
    myfile.open ("./files_exported/list_nodes.pdf");
    myfile << content;
    myfile.close();
}

/*!Export to a file web.pdf the content of the option web*/
void export_web(string content){
    ofstream myfile;
    myfile.open ("./files_exported/web.pdf");
    myfile << content;
    myfile.close();
}
