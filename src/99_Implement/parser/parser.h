#ifndef __PARSER_H__
#define __PARSER_H__

using namespace std;

struct List;
struct Node;

void insert_tag(List *general_labels);
void colect_caracter(char caracter);
void is_not_key(char caracter);
void rating_caracter(char caracter,List *general_tags);
void start_parse_file(char *docname, List *general_tags);
void parse_file(List *ls, FILE *pf);
void print_events_on_file(List *general_tags);
Node search_node_father(List *tags_list);

void omite_pieces(string content);
void find_all_comments(string content, int pos);
bool parse_that_position(int position);
void find_doctype(string content, int pos);
void find_all_styles(string content, int pos);

#endif
