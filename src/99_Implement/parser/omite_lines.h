#include <string>

#ifndef __OMITE_LINES_H__
#define __OMITE_LINES_H__

using namespace std;

void omite_pieces(string content);
void find_gap_tags(string content, int pos, string open_tag, string close_tag);
void find_all_comments(string content, int pos);
void find_doctype(string content, int pos);
bool parse_that_position(int position);

#endif

