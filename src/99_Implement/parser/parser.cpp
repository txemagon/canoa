#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <cstring>
#include <fstream>

//Own documents
#include "definitions.h"

using namespace std;

//VARIABLES BANDERA
bool open_tag = false;
bool inside_tag = false;    //<html inside_label>
bool on_tag = false;        //<html> On label </html>
bool is_a_comment = false;
bool is_attribute = false;

//Buffers
string general_content;
string tag;
string father_tag;
string attribute;

//Usefull variables
char prev_char;
int cnt_id = 0;
int pos_caracter = 0;

/*!launcher to start parsing document caracter by caracter
 * */
void start_parse_file(char *docname, List *tags_list){
    string file_name = "pagina.html";
    ifstream ifs(file_name);
    string content ((istreambuf_iterator<char>(ifs) ), (istreambuf_iterator<char>()));


    omite_pieces(content);    /*omite_lines.cpp*/

    while (pos_caracter < content.size()) {
        char ch;
        ch = content.at(pos_caracter);
        if(parse_that_position(pos_caracter) == true && ch != '\n' && ch != '\t' && ch != '\r'){
            rating_caracter(ch, tags_list);
        }
        if(pos_caracter != 0)
            prev_char = content.at(pos_caracter - 1);

        pos_caracter++;
    }
}

/*! it works like a controller depending the type of caracter
 * */
void rating_caracter(char caracter,List *general_tags){
    if(caracter == '<'){
        if(on_tag == true){
            Node *temp = new Node();
            temp = general_tags->ult_nodo;
            temp->content += general_content;
        }
        inside_tag = true;
        open_tag = true;
        tag = "";
        return;
    }
    if(caracter == '>'){
        if(open_tag == false)
            on_tag = true;

        inside_tag = false;
        is_attribute = false;
        insert_tag(general_tags);
        return;
    }else{
        is_not_key(caracter);
    }
}

void is_not_key(char caracter){
    if(open_tag == true && caracter == '/' && is_attribute == false){
        on_tag = false;
        open_tag = false;
        return;
    }
    if(caracter == ' ' && inside_tag == true){
        is_attribute = true;
    }
    else {
        colect_caracter(caracter);
        return;
    }

}

/*!Depend the options we insert into a attrubute, tag or general_content
 * */
void colect_caracter(char caracter){
    if(is_attribute == true){
        attribute += caracter;
        return;
    }
    if(inside_tag == true)
        tag += caracter;
    else{
        general_content += caracter;
    }
}

/*! This function generate a temporal Node and then insert the concrer Node
 * into a List called general_tags
 * */
void insert_tag(List *general_tags){
    Node *node = new Node();

    if(tag == "!DOCTYPE"){
        //We insert the doctype node
        return;
    }
    if(tag[0] == '!'){
        //Is a comment
        return;
    }
    if(tag == "style")
        return;

    string temp_tag = "";
    temp_tag = tag;
    int size = tag.size();
    tag = "";

    for(int cnt = 0; cnt < size; cnt++){
        if(temp_tag.at(cnt) != ' ')
            tag += temp_tag.at(cnt);
    }

    node->tag_name = tag;
    node->id = cnt_id++;
    node->attribute = attribute;
    attribute = "";


    if(general_content != ""){
        Node *temp = new Node();
        //*temp = general_tags->get_node_by_type_and_name("open", tag);
        /*
    Node *temp = new Node();
     temp = this->ult_nodo;
     while(temp != NULL){
         if(temp->tag_name == tag && temp->tag_type == type)
             return *temp;

        temp = temp->prev;
     }
     */

        node->content = general_content;
        general_content = "";
    }

    if(open_tag == true)
        node->tag_type = "open";
    else
        node->tag_type = "close";

    general_tags->insert_node_at_end(node);
    open_tag = false;
}

/*
void insert_tag_child(List *tags_list){
    Node *node_father = new Node();
    *node_father = search_node_father(tags_list);
    if(node_father != NULL){
        List *child_list = new List();
        insert_tag(child_list);
        node_father->childList = child_list;
    }
}
*/

/*
Node search_node_father(List *tags_list){
    Node *temp = new Node();
    temp = tags_list->ult_nodo;
    while(temp != NULL){
        if(temp->tag_name == father_tag && temp->tag_type == "open")
            return *temp;

        temp = temp->prev;
    }
    return NULL;
}
*/



/*Funcion que imprime eventos de las etiquetas en un txt*/
/*
   void print_events_on_file(List *general_tags){
   Label *temp = new Label();
   std::ofstream outfile("events.txt");

   if(general_tags->ult_label == NULL){
   printf("error");
   return;
   }

   temp = general_tags->ult_label;
   while(temp != NULL){

   outfile << temp->content + " - " + temp->type << std::endl;
//system(date +%T:%N | cut -b-11)
temp = temp->prev;
}

outfile.close();
}
*/
