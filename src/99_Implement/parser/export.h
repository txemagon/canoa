#ifndef __EXPORT_H__
#define __EXPORT_H__

#include <string>

using namespace std;

void export_dom_struct(string content);
void export_list_nodes(string content);
void export_web(string content);

#endif
