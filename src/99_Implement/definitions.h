#ifndef __DEFINITIONS_H__
#define __DEFINITIONS_H__
//Dom docs
#include "dom_struct/list.h"
#include "dom_struct/node.h"
//Parser docs
#include "parser/parser.h"
#include "parser/legacy.h"
#include "parser/checker.h"
#include "parser/outprint.h"
#include "parser/export.h"
#include "parser/purgate.h"
//Get HTML
#include "get_html/get_html.h"
//Render
#include "render/tags_controller.h"
//Interface
#include "interface/menu.h"
#include "interface/interface.h"
#include "interface/window.h"

#endif
